AWS Base
=========

Installs base packages that should be on every managed EC2 instance.

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - aws_base

License
-------

BSD

Author Information
------------------

Justin van Heerde (jvanheerde@gmail.com)
