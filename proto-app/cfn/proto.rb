CloudFormation {
	Description("Prototype app template")
	AWSTemplateFormatVersion("2010-09-09")

	# Parameters
	Parameter(:VpcId) {
        Description "ID of an existing VPC"
        ConstraintDescription "Must be an existing VPC."
        Type "AWS::EC2::VPC::Id"
    }

    Parameter(:KeyName) {
        Description "Name of an existing EC2 KeyPair to enable SSH access to the instance"
        ConstraintDescription "Must be the name of an existing EC2 KeyPair."
        Type "AWS::EC2::KeyPair::KeyName"
    }

    Parameter(:DBUser) {
    	Description "The database admin account username"
    	Type "String"
    	MinLength "1"
    	MaxLength "16"
    	AllowedPattern "[a-zA-Z][a-zA-Z0-9]*"
    	ConstraintDescription "must begin with a letter and contain only alphanumeric characters."
    }

    Parameter(:DBPassword) {
    	NoEcho true
    	Description "The database admin account password"
    	Type "String"
    	MinLength "1"
    	MaxLength "41"
    	AllowedPattern "[a-zA-Z0-9]*"
    	ConstraintDescription "must contain only alphanumeric characters."
    }

    Parameter(:DBInstanceClass) {
    	Default "db.t2.micro"
    	Description "Database instance class"
    	Type "String"
    	AllowedValues [
    	    "db.t2.micro", "db.t2.small", "db.t2.medium", "db.t2.large",
    	    "db.m4.large", "db.m4.xlarge", "db.m4.2xlarge", "db.m4.4xlarge", "db.m4.10xlarge",
    	    "db.m3.medium", "db.m3.large", "db.m3.xlarge", "db.m3.2xlarge",
    	    "db.r3.large", "db.r3.xlarge", "db.r3.2xlarge", "db.r3.4xlarge", "rb.r3.8xlarge"]
    	ConstraintDescription "must select a valid database instance type."
    }

    Parameter(:DBAllocatedStorage) {
    	Default "5"
    	Description "The size of the database (Gb)"
    	Type "Number"
    	MinValue "5"
    	MaxValue "1024"
    	ConstraintDescription "must be between 5 and 1024Gb."
    }

    Parameter(:WebInstanceType) {
    	Type "String"
    	Description "Web server EC2 instance type"
    	Default "t2.micro"
    	AllowedValues [
    	    "t2.micro", "t2.small", "t2.medium",
    	    "m3.medium", "m3.large", "m3.xlarge", "m3.2xlarge",
    	    "c3.large", "c3.xlarge", "c3.2xlarge", "c3.4xlarge", "c3.8xlarge",
    	    "r3.large", "r3.xlarge", "r3.2xlarge", "r3.4xlarge", "r3.8xlarge",
    	    "i2.xlarge", "i2.2xlarge", "i2.4xlarge", "i2.8xlarge",
    	    "hi1.4xlarge",
    	    "hs1.8xlarge",
    	    "cr1.8xlarge",
    	    "cc2.8xlarge"
    	]
    	ConstraintDescription "must be a valid EC2 instance type."
    }

    Parameter(:PublicSubnets) {
    	Type "List<AWS::EC2::Subnet::Id>"
    	Description "Public subnet-ids for this environment"
    }

    Parameter(:PrivateSubnets) {
    	Type "List<AWS::EC2::Subnet::Id>"
    	Description "Private subnet-ids for this environment"
    }

    Parameter(:DBSubnetGroup) {
    	Type "String"
    	Description "DB subnet group name"
    }

    Parameter(:MgmtSecurityGroup) {
    	Type "<AWS::EC2::SecurityGroup::Id>"
    	Description "Management security group that all instances should use"
    }

    Mapping(:AWSInstanceType2Arch, {
        't1.micro' => {'Arch'=>'PV64'}, 't2.micro' => {'Arch'=>'HVM64'}, 't2.small' => {'Arch'=>'HVM64'},
        't2.medium'=> {'Arch'=>'HVM64'}, 'm1.small' => {'Arch'=>'PV64'}, 'm1.medium' => {'Arch'=>'PV64'},
        'm1.large' => {'Arch'=>'PV64'}, 'm1.xlarge' => {'Arch'=>'PV64'}, 'm2.xlarge' => {'Arch'=>'PV64'},
        'm2.2xlarge' => {'Arch'=>'PV64'}, 'm2.4xlarge' => {'Arch'=>'PV64'}, 'm3.medium' => {'Arch'=>'HVM64'},
        'm3.large' => {'Arch'=>'HVM64'}, 'm3.xlarge' => {'Arch'=>'HVM64'}, 'm3.2xlarge' => {'Arch'=>'HVM64'},
        'c1.medium' => {'Arch'=>'PV64'}, 'c1.xlarge' => {'Arch'=>'PV64'}, 'c3.large' => {'Arch'=>'HVM64'},
        'c3.xlarge' => {'Arch'=>'HVM64'}, 'c3.2xlarge' => {'Arch'=>'HVM64'}, 'c3.4xlarge' => {'Arch'=>'HVM64'},
        'c3.8xlarge' => {'Arch'=>'HVM64'}, 'g2.2xlarge' => {'Arch'=>'HVMG2'},'r3.large' => {'Arch'=>'HVM64'},
        'r3.xlarge' => {'Arch'=>'HVM64'}, 'r3.2xlarge' => {'Arch'=>'HVM64'}, 'r3.4xlarge' => {'Arch'=>'HVM64'},
        'r3.8xlarge' => {'Arch'=>'HVM64'}, 'i2.xlarge' => {'Arch'=>'HVM64'}, 'i2.2xlarge' => {'Arch'=>'HVM64'},
        'i2.4xlarge' => {'Arch'=>'HVM64'}, 'i2.8xlarge' => {'Arch'=>'HVM64'}, 'hi1.4xlarge' => {'Arch'=>'HVM64'},
        'hs1.8xlarge' => {'Arch'=>'HVM64'}, 'cr1.8xlarge' => {'Arch'=>'HVM64'}, 'cc2.8xlarge' => {'Arch'=>'HVM64'},
    })
    
    Mapping(:AWSRegionArch2Redhat72AMI, {
        'us-east-1'     => {'HVM64' => 'ami-2051294a'},
        'ap-northeast-1'=> {'HVM64' => 'ami-0dd8f963'},
        'sa-east-1'     => {'HVM64' => 'ami-27b3094b'},
        'ap-southeast-1'=> {'HVM64' => 'ami-3f03c55c'},
        'ap-southeast-2'=> {'HVM64' => 'ami-e0c19f83'},
        'us-west-2'     => {'HVM64' => 'ami-775e4f16'},
        'us-west-1'     => {'HVM64' => 'ami-d1315fb1'},
        'eu-central-1'  => {'HVM64' => 'ami-875042eb'},
        'eu-west-1'     => {'HVM64' => 'ami-8b8c57f8'}
    })
    
    EC2_SecurityGroup(:ProtoWebSecurityGroup) {
    	GroupDescription "Web security group"
    	VpcId Ref(:VpcId)
    	
    	web_ingress_rules = []
    	WEB_ACCESS.each { |a|
    		WEB_PORTS.each { |p|
    			web_ingress_rules << {"CidrIp" => a, "FromPort" => p, "IpProtocol" => "tcp", "ToPort" => p }
    		}
    	}
    	Property("SecurityGroupIngress", web_ingress_rules)
    	Property("SecurityGroupEgress", [
    		{ 
    			"SourceSecurityGroupId" => Ref(:ProtoDBSecurityGroup),
    			"FromPort" => 3306,
    			"ToPort" => 3306,
    			"IpProtocol" => "tcp"
    		}
    	])
    }

    EC2_SecurityGroup(:ProtoDBSecurityGroup) {
    	GroupDescription "DB security group"
    	VpcId Ref(:VpcId)
    }

    EC2_SecurityGroupIngress(:ProtoDBSecurityGroupWebIngress) {
        SourceSecurityGroupId Ref(:ProtoWebSecurityGroup)
        FromPort 3306
        ToPort 3306
        IpProtocol "tcp"
    }

    RDS_DBInstance(:DBInstance) {
    	AllocatedStorage Ref(:DBAllocatedStorage)
    	DBInstanceClass Ref(:DBInstanceClass)
    	Engine "MySQL"
    	EngineVersion "5.6"
    	MasterUsername Ref(:DBUser)
    	MasterUserPassword Ref(:DBPassword)
    	DBSubnetGroupName Ref(:DBSubnetGroup)
    	VPCSecurityGroups [Ref(:ProtoDBSecurityGroup), Ref(:MgmtSecurityGroup)]
    	DeletionPolicy "Snapshot"
    	addTag("Role", "rds_mysql_db")
    }

    EC2_Instance(:ProtoWebInstance) {
        ImageId FnFindInMap(:AWSRegionArch2Redhat72AMI, Ref("AWS::Region"),
            FnFindInMap(:AWSInstanceType2Arch, Ref(:WebInstanceType), "Arch"))
        InstanceType Ref(:WebInstanceType)
        KeyName Ref(:KeyName)
	    SubnetId FnSelect(0, Ref(:PublicSubnets))
        SecurityGroupIds [Ref(:ProtoWebSecurityGroup)]
        addTag("Role", "proto_web")
    }

    # this should actually not use ElasticIPs and update Route53 directly
    EIP(:ProtoWebEIP) {
        Domain "vpc"
        InstanceId Ref(:ProtoWebInstance)
    }

    Output(:ProtoWebPublicDnsName) {
        Description "Public DNS for webserver"
        Value FnGetAtt(:ProtoWebInstance, "PublicDnsName")
    }

    Output(:ProtoWebPublicIp) {
    	Description "Public IP for webserver"
    	Value Ref(:ProtoWebEIP)
    }

    Output(:DBInstanceEndpoint) {
        Description "Public IP for webserver"
        Value FnGetAtt(:DBInstance, "Endpoint.Address")
    }

    Output(:DBInstancePort) {
        Description "Port for db instance"
        Value FnGetAtt(:DBInstance, "Endpoint.Port")
    }
}