#!/usr/bin/env ruby
require 'aws-sdk'
require 'optparse'
require 'json'

def usage
	puts "Script to find management parameters for use with CloudFormation"
	puts "Usage: #{$0} <stack-name>\n"
end

if(ARGV.length != 1)
	usage
	exit
end

stack_name = ARGV[0]

params = [:MgmtVpcId, :MgmtRouteTable, :MgmtVpcCidr, :BastionSecurityGroup]
cloudformation = Aws::CloudFormation::Client.new
s = cloudformation.describe_stacks.stacks.find { |x| x.stack_name == stack_name }

cf_params = []
if s
	params.each do |p|
		o = s[:outputs].find { |x| x["output_key"] == p.to_s }
		cf_params << { "ParameterKey" => p, "ParameterValue" => o[:output_value] } if o
	end
else
	puts "Stack #{stack_name} not found!"
	exit
end

puts JSON.pretty_generate(cf_params)