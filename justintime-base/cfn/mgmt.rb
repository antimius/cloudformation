CloudFormation {
    BASTIONINSTANCETYPE ||= "t2.micro"
    LOGINSTANCETYPE ||= "t2.micro"
    SSH_HOSTS ||= []

	Description "Management infrastructure - Contains 1 VPC: MgmtVpc"
	AWSTemplateFormatVersion "2010-09-09"

    # Mappings
    Mapping(:AWSInstanceType2Arch, {
        't1.micro' => {'Arch'=>'PV64'}, 't2.micro' => {'Arch'=>'HVM64'}, 't2.small' => {'Arch'=>'HVM64'},
        't2.medium'=> {'Arch'=>'HVM64'}, 'm1.small' => {'Arch'=>'PV64'}, 'm1.medium' => {'Arch'=>'PV64'},
        'm1.large' => {'Arch'=>'PV64'}, 'm1.xlarge' => {'Arch'=>'PV64'}, 'm2.xlarge' => {'Arch'=>'PV64'},
        'm2.2xlarge' => {'Arch'=>'PV64'}, 'm2.4xlarge' => {'Arch'=>'PV64'}, 'm3.medium' => {'Arch'=>'HVM64'},
        'm3.large' => {'Arch'=>'HVM64'}, 'm3.xlarge' => {'Arch'=>'HVM64'}, 'm3.2xlarge' => {'Arch'=>'HVM64'},
        'c1.medium' => {'Arch'=>'PV64'}, 'c1.xlarge' => {'Arch'=>'PV64'}, 'c3.large' => {'Arch'=>'HVM64'},
        'c3.xlarge' => {'Arch'=>'HVM64'}, 'c3.2xlarge' => {'Arch'=>'HVM64'}, 'c3.4xlarge' => {'Arch'=>'HVM64'},
        'c3.8xlarge' => {'Arch'=>'HVM64'}, 'g2.2xlarge' => {'Arch'=>'HVMG2'},'r3.large' => {'Arch'=>'HVM64'},
        'r3.xlarge' => {'Arch'=>'HVM64'}, 'r3.2xlarge' => {'Arch'=>'HVM64'}, 'r3.4xlarge' => {'Arch'=>'HVM64'},
        'r3.8xlarge' => {'Arch'=>'HVM64'}, 'i2.xlarge' => {'Arch'=>'HVM64'}, 'i2.2xlarge' => {'Arch'=>'HVM64'},
        'i2.4xlarge' => {'Arch'=>'HVM64'}, 'i2.8xlarge' => {'Arch'=>'HVM64'}, 'hi1.4xlarge' => {'Arch'=>'HVM64'},
        'hs1.8xlarge' => {'Arch'=>'HVM64'}, 'cr1.8xlarge' => {'Arch'=>'HVM64'}, 'cc2.8xlarge' => {'Arch'=>'HVM64'},
    })
    
    Mapping(:AWSRegionArch2Redhat72AMI, {
        'us-east-1'     => {'HVM64' => 'ami-2051294a'},
        'ap-northeast-1'=> {'HVM64' => 'ami-0dd8f963'},
        'sa-east-1'     => {'HVM64' => 'ami-27b3094b'},
        'ap-southeast-1'=> {'HVM64' => 'ami-3f03c55c'},
        'ap-southeast-2'=> {'HVM64' => 'ami-e0c19f83'},
        'us-west-2'     => {'HVM64' => 'ami-775e4f16'},
        'us-west-1'     => {'HVM64' => 'ami-d1315fb1'},
        'eu-central-1'  => {'HVM64' => 'ami-875042eb'},
        'eu-west-1'     => {'HVM64' => 'ami-8b8c57f8'}
    })

    Mapping(:Region2VpcCidr, {
        "eu-central-1" => {
            "MgmtVpcCidr" => "10.254.0.0/16",
            "MgmtSubnetCidr" => "10.254.1.0/24"
        },
        "eu-west-1" => {
            "MgmtVpcCidr" => "10.253.0.0/16",
            "MgmtSubnetCidr" => "10.253.1.0/24"
        }
    })

    # Parameters
    Parameter(:KeyName) {
        Description "Name of an existing EC2 KeyPair to enable SSH access to the instance"
        ConstraintDescription "Must be the name of an existing EC2 KeyPair."
        Type "AWS::EC2::KeyPair::KeyName"
    }

    # Resources
    # VPC + subnets + internet gateway + route table
	VPC(:MgmtVpc) {
		CidrBlock FnFindInMap(:Region2VpcCidr, Ref("AWS::Region"), "MgmtVpcCidr")
    }

	Subnet(:MgmtSubnet) {
		VpcId Ref(:MgmtVpc)
        MapPublicIpOnLaunch true
		CidrBlock FnFindInMap(:Region2VpcCidr, Ref("AWS::Region"), "MgmtSubnetCidr")		
	}

	InternetGateway(:InternetGateway) { }

    VPCGatewayAttachment(:GatewayAttachment) {
    	VpcId Ref(:MgmtVpc)
    	InternetGatewayId Ref(:InternetGateway)
    }

    RouteTable(:MgmtRouteTable) {
    	VpcId Ref(:MgmtVpc)
    }

    Route(:PublicRoute) {
    	DependsOn "GatewayAttachment"
    	RouteTableId Ref(:MgmtRouteTable)
    	DestinationCidrBlock "0.0.0.0/0"
    	GatewayId Ref(:InternetGateway)
    }

    SubnetRouteTableAssociation(:MgmtSubnetRouteTableAssociation) {
        RouteTableId Ref(:MgmtRouteTable)
        SubnetId Ref(:MgmtSubnet)
    }

    # EIPs
    EIP(:BastionIPAddress) {
        DependsOn "GatewayAttachment"
        Domain "vpc"
        InstanceId Ref(:BastionHost)
    }

    # Security Groups
    EC2_SecurityGroup(:BastionSecurityGroup) {
        GroupDescription "Enable access to the Bastion host"
        VpcId Ref(:MgmtVpc)
        ssh_ingress_rules = []
        
        # SSH hosts are defined in mgmt-params.yaml
        SSH_HOSTS.each { |h|
            ssh_ingress_rules << {"CidrIp" => h, "FromPort" => "22", "IpProtocol" => "tcp", "ToPort" => "22"}
        }
        
        Property("SecurityGroupIngress", ssh_ingress_rules)
        Property("SecurityGroupEgress", [
            {
                "CidrIp"     => "0.0.0.0/0",
                "FromPort"   => "22",
                "IpProtocol" => "tcp",
                "ToPort"     => "22"
            },
            {
                "CidrIp"    => "0.0.0.0/0",
                "FromPort"  => "80",
                "IpProtocol"=> "tcp",
                "ToPort"    => "80"
            },
            {
                "CidrIp"    => "0.0.0.0/0",
                "FromPort"  => "443",
                "IpProtocol"=> "tcp",
                "ToPort"    => "443"
            }
        ])
    }

    EC2_SecurityGroup(:LogManagementSecurityGroup) {
        GroupDescription "Enable access to log management ports"
        VpcId Ref(:MgmtVpc)
        
        Property("SecurityGroupIngress", [
            {
                "SourceSecurityGroupId" => Ref("BastionSecurityGroup"),
                "FromPort"      => "22",
                "IpProtocol"    => "tcp",
                "ToPort"        => "22"
            }]
        )
            
        Property("SecurityGroupEgress", [
            {
                "CidrIp"    => "0.0.0.0/0",
                "FromPort"  => "80",
                "IpProtocol"=> "tcp",
                "ToPort"    => "80"
            },
            {
                "CidrIp"    => "0.0.0.0/0",
                "FromPort"  => "443",
                "IpProtocol"=> "tcp",
                "ToPort"    => "443"
            }]
        )
    }

    EC2_SecurityGroup(:PulpSecurityGroup) {
        GroupDescription "Enable access to log management ports"
        VpcId Ref(:MgmtVpc)
        
        Property("SecurityGroupIngress", [
            {
                "SourceSecurityGroupId" => Ref("BastionSecurityGroup"),
                "FromPort"      => "22",
                "IpProtocol"    => "tcp",
                "ToPort"        => "22"
            },
            {
                "CidrIp"    => "0.0.0.0/0",
                "FromPort"  => "80",
                "IpProtocol"=> "tcp",
                "ToPort"    => "80"
            },
            {
                "CidrIp"    => "0.0.0.0/0",
                "FromPort"  => "443",
                "IpProtocol"=> "tcp",
                "ToPort"    => "443"
            }
        ])
            
        Property("SecurityGroupEgress", [
            {
                "CidrIp"    => "0.0.0.0/0",
                "FromPort"  => "80",
                "IpProtocol"=> "tcp",
                "ToPort"    => "80"
            },
            {
                "CidrIp"    => "0.0.0.0/0",
                "FromPort"  => "443",
                "IpProtocol"=> "tcp",
                "ToPort"    => "443"
            }
        ])
    }

    # Instances
    EC2_Instance(:BastionHost) {
        InstanceType BASTIONINSTANCETYPE
        KeyName Ref(:KeyName)
        SubnetId Ref(:MgmtSubnet)
        ImageId FnFindInMap(:AWSRegionArch2Redhat72AMI, Ref("AWS::Region"),
            FnFindInMap(:AWSInstanceType2Arch, BASTIONINSTANCETYPE, "Arch"))
        SecurityGroupIds [Ref(:BastionSecurityGroup)]
        addTag("Role", "bastion")
    }

    EC2_Instance(:LogHost) {
        InstanceType LOGINSTANCETYPE
        KeyName Ref(:KeyName)
        SubnetId Ref(:MgmtSubnet)
        ImageId FnFindInMap(:AWSRegionArch2Redhat72AMI, Ref("AWS::Region"),
            FnFindInMap(:AWSInstanceType2Arch, LOGINSTANCETYPE, "Arch"))
        SecurityGroupIds [Ref(:LogManagementSecurityGroup)]
        addTag("Role", "log_mgmt")
    }

    EC2_Instance(:PulpHost) {
        InstanceType BASTIONINSTANCETYPE
        KeyName Ref(:KeyName)
        SubnetId Ref(:MgmtSubnet)
        ImageId FnFindInMap(:AWSRegionArch2Redhat72AMI, Ref("AWS::Region"),
            FnFindInMap(:AWSInstanceType2Arch, BASTIONINSTANCETYPE, "Arch"))
        SecurityGroupIds [Ref(:PulpSecurityGroup)]
        addTag("Role", "pulp")
    }

    # Outputs
    Output(:MgmtVpcId) {
        Description "VpcId of the MgmtVpc"
        Value Ref(:MgmtVpc)
    }

    Output(:MgmtVpcCidr) {
        Description "Cidr block of the MgmtVpc"
        Value FnGetAtt(:MgmtVpc, "CidrBlock")
    }

    Output(:MgmtRouteTable) {
        Description "Route table id for the mgmt route table (for VPC peering)"
        Value Ref(:MgmtRouteTable)
    }

    Output(:MgmtRouteTable) {
        Description "Route table id for the mgmt route table (for VPC peering)"
        Value Ref(:MgmtRouteTable)
    }

    Output(:BastionIp) {
        Description "Bastion elastic IP"
        Value Ref(:BastionIPAddress)
    }

    Output(:BastionSecurityGroup) {
        Description "Bastion security group id"
        Value Ref(:BastionSecurityGroup)
    }

    Output(:LogHostIp) {
        Description "Log host private IP"
        Value FnGetAtt(:LogHost, "PrivateIp")
    }

    Output(:PulpHostIp) {
        Description "Pulp host private IP"
        Value FnGetAtt(:PulpHost, "PrivateIp")
    }
}