CloudFormation {
	Description("Basic VPC configuration template\nContains 2 VPCs => PROD, STAGING")
	AWSTemplateFormatVersion("2010-09-09")

	# Parameters
	CidrRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$"
	Parameter(:MgmtVpcId) {
        Description "ID of an existing management VPC"
        ConstraintDescription "Must be an existing VPC."
        Type "AWS::EC2::VPC::Id"
    }

    Parameter(:MgmtRouteTable) {
        Description "Management VPC route table"
        ConstraintDescription "Must be an existing route table."
        Type "String"
        AllowedPattern "rtb-[0-9a-f]{8}"
        ConstraintDescription "Must be a valid route table id."
    }

    Parameter(:MgmtVpcCidr) {
        Description "Management VPC Cidr"
        ConstraintDescription "Must be an existing management VPC cidr."
        Type "String"
        AllowedPattern CidrRegex
    }

    Parameter(:KeyName) {
        Description "Name of an existing EC2 KeyPair to enable SSH access to the instance"
        ConstraintDescription "Must be the name of an existing EC2 KeyPair."
        Type "AWS::EC2::KeyPair::KeyName"
    }

    Parameter(:BastionHostCidr) {
    	Description "IP for the bastion host in the mgmt VPC"
    	ConstraintDescription "Must be a valid CIDR"
    	Type "String"
    	AllowedPattern CidrRegex
    }

    Parameter(:LogManagementCidr) {
    	Description "IP for the log management host in the mgmt VPC"
    	ConstraintDescription "Must be a valid CIDR"
    	Type "String"
    	AllowedPattern CidrRegex
    }

    Parameter(:PulpHostCidr) {
    	Description "IP for the pulp host in the mgmt VPC"
    	ConstraintDescription "Must be a valid CIDR"
    	Type "String"
    	AllowedPattern CidrRegex
    }

    # Userdata
    userdata_nat = FnBase64(
    	FnJoin(
    			'',
        		[
  		      		"#!/bin/bash -v\n",
					"yum update -y aws*\n",
					"# Configure iptables\n",
					"/sbin/iptables -t nat -A POSTROUTING -o eth0 -s 0.0.0.0/0 -j MASQUERADE\n",
					"# Configure ip forwarding and redirects\n",
					"echo 1 >  /proc/sys/net/ipv4/ip_forward && echo 0 >  /proc/sys/net/ipv4/conf/eth0/send_redirects\n",
					"mkdir -p /etc/sysctl.d/\n",
					"cat <<EOF > /etc/sysctl.d/nat.conf\n",
					"net.ipv4.ip_forward = 1\n",
					"net.ipv4.conf.eth0.send_redirects = 0\n",
					"EOF\n",
					"/sbin/iptables-save > /etc/sysconfig/iptables\n",
					"\n"
				]
		)
    )

    # Mappings
	# We don't HAVE to have unique subnet CIDRs per VPC,
	# but this way we're sure we don't overlap - even across regions (you never know...)
	# We might want to be able to hook in VGWs
	Mapping("Region2VpcCidr", {
        "eu-central-1" => {
            "ProdVpcCidr" => "10.4.0.0/16",
            "ProdPublicSubnetACidr" => "10.4.1.0/24",
            "ProdPublicSubnetBCidr" => "10.4.2.0/24",
            "ProdPrivateSubnetACidr" => "10.4.3.0/24",
            "ProdPrivateSubnetBCidr" => "10.4.4.0/24",
            "StagingVpcCidr"  => "10.5.0.0/16",
            "StagingPublicSubnetACidr" => "10.5.1.0/24",
            "StagingPublicSubnetBCidr" => "10.5.2.0/24",
            "StagingPrivateSubnetACidr" => "10.5.3.0/24",
            "StagingPrivateSubnetBCidr" => "10.5.4.0/24"
        },
        "eu-west-1" => {
            "ProdVpcCidr"  => "10.6.0.0/16",
            "ProdPublicSubnetACidr" => "10.6.1.0/24",
            "ProdPublicSubnetBCidr" => "10.6.2.0/24",
            "ProdPrivateSubnetACidr" => "10.6.3.0/24",
            "ProdPrivateSubnetBCidr" => "10.6.4.0/24",
            "StagingVpcCidr"  => "10.7.0.0/16",
            "StagingPublicSubnetACidr" => "10.7.1.0/24",
            "StagingPublicSubnetBCidr" => "10.7.2.0/24",
            "StagingPrivateSubnetACidr" => "10.7.3.0/24",
            "StagingPrivateSubnetBCidr" => "10.7.4.0/24"
        }
    })

    Mapping("NatRegionMap", {
    	"us-east-1" => { "AMI" => "ami-184dc970" },
    	"us-west-1" => { "AMI" => "ami-a98396ec" },
    	"us-west-2" => { "AMI" => "ami-290f4119" },
    	"eu-central-1" => { "AMI" => "ami-ae380eb3" },
    	"eu-west-1" => { "AMI" => "ami-14913f63" },
    	"ap-southeast-1" => { "AMI" => "ami-6aa38238" },
    	"ap-southeast-2" => { "AMI" => "ami-893f53b3" },
    	"ap-northeast-1" => { "AMI" => "ami-27d6e626" },
    	"sa-east-1" => { "AMI" => "ami-8122969c" },
    })

	# Resources
	zones = ("A".."B").to_a
	vpcs = ["Prod", "Staging"]
	net_tiers = ["Public", "Private"]
	# VPCs, subnets, dbsubnetgroups
	vpcs.each { |e|
		# VPCs
		VPC(:"#{e}Vpc") {
			CidrBlock FnFindInMap("Region2VpcCidr", Ref("AWS::Region"), "#{e}VpcCidr")
		}
		
		# Subnets
		net_tiers.each { |p|
			zones.each { |z|
				Subnet(:"#{e}#{p}Subnet#{z}") {
					VpcId Ref(:"#{e}Vpc")
					AvailabilityZone FnSelect(z.ord - "A".ord, FnGetAZs(""))
					MapPublicIpOnLaunch true if p == "Public"
					CidrBlock FnFindInMap("Region2VpcCidr", Ref("AWS::Region"),
						"#{e}#{p}Subnet#{z}Cidr")
				}
			}
		}
		
		# DBSubnetGroups
		DBSubnetGroup(:"#{e}DBSubnetGroup") {
			DBSubnetGroupDescription "#{e} DB subnet group"
			SubnetIds Array.new(zones.length) { |i| Ref(:"#{e}PrivateSubnet#{zones[i]}") }
		}

		# VPC Peering
		VPCPeeringConnection(:"Mgmt#{e}Peering") {
			VpcId Ref(:"#{e}Vpc")
			PeerVpcId Ref(:MgmtVpcId)
		}

		# Internet Gateways + attachments
		InternetGateway(:"#{e}InternetGateway") { }

		VPCGatewayAttachment(:"#{e}GatewayAttachment") {
			VpcId Ref(:"#{e}Vpc")
			InternetGatewayId Ref(:"#{e}InternetGateway")
		}

		# route tables + routes
		net_tiers.each { |p|
			RouteTable(:"#{e}#{p}RouteTable") {
				VpcId Ref(:"#{e}Vpc")
			}
			
			# VPC Peering Routes
			Route(:"Mgmt#{e}#{p}Route") {
				RouteTableId Ref(:"#{e}#{p}RouteTable")
				DestinationCidrBlock Ref(:"MgmtVpcCidr")
				VpcPeeringConnectionId Ref(:"Mgmt#{e}Peering")
			}
		}

		# subnet route table associations
		net_tiers.each { |p|
			zones.each { |z|
				SubnetRouteTableAssociation(:"#{e}#{p}SubnetRouteTableAssociation#{z}") {
					RouteTableId Ref(:"#{e}#{p}RouteTable")
					SubnetId Ref(:"#{e}#{p}Subnet#{z}")
				}
			}
		}

		# route internet traffic to igw (for public subnets)
		Route(:"#{e}PublicRoute") {
			DependsOn :"#{e}GatewayAttachment"
			RouteTableId Ref(:"#{e}PublicRouteTable")
			DestinationCidrBlock "0.0.0.0/0"
			GatewayId Ref(:"#{e}InternetGateway")
		}

		# create route in existing management VPC route table back to our VPCs
		Route(:"#{e}MgmtRoute") {
			RouteTableId Ref(:"MgmtRouteTable")
			DestinationCidrBlock FnFindInMap("Region2VpcCidr", Ref("AWS::Region"), "#{e}VpcCidr")
			VpcPeeringConnectionId Ref(:"Mgmt#{e}Peering")
		}

		# NAT security group
		EC2_SecurityGroup(:"#{e}NatSecurityGroup") {
		    GroupDescription "Enable access from NAT instance to internet"
		    VpcId Ref(:"#{e}Vpc")
		    
		    # this has to be declared explicitly as a Property because of 
		    Property("SecurityGroupIngress", [
		        {
		            "CidrIp" => FnFindInMap("Region2VpcCidr", Ref("AWS::Region"), "#{e}VpcCidr"),
		            "FromPort"      => "0",
		            "IpProtocol"    => "-1",
		            "ToPort"        => "65535"
		        }
		    ])
		        
		    Property("SecurityGroupEgress", [
		        {
		            "CidrIp"    => "0.0.0.0/0",
		            "FromPort"  => "0",
		            "ToPort"    => "65535",
		            "IpProtocol"=> "-1"
		        }
		    ])
		}

		# NAT instances in public networks
		EC2_Instance(:"#{e}NatInstance") {
			InstanceType "t2.micro"
		    KeyName Ref(:KeyName)
		    SubnetId Ref(:"#{e}PublicSubnet#{zones[0]}")
		    ImageId FnFindInMap("NatRegionMap", Ref("AWS::Region"), "AMI")
		    SourceDestCheck false
		    SecurityGroupIds [ Ref(:"#{e}NatSecurityGroup") ]
		    addTag("Role", "nat")
		    UserData userdata_nat
		}

		# Route to NAT instances from private subnets
		Route(:"#{e}NatRoute") {
			DependsOn :"#{e}NatInstance"
			RouteTableId Ref(:"#{e}PrivateRouteTable")
			DestinationCidrBlock "0.0.0.0/0"
			InstanceId Ref(:"#{e}NatInstance")
		}

		# Management security group
		EC2_SecurityGroup(:"#{e}MgmtSecurityGroup") {
		    GroupDescription "Enable access from and to management hosts"
		    VpcId Ref(:"#{e}Vpc")
		    
		    # this has to be declared explicitly as a Property because of a bug in cfndsl
		    # https://github.com/stevenjack/cfndsl/issues/72
		    # TODO: refactor all this stuff so we can just generate everything including NACLs
		    Property("SecurityGroupIngress",[
		    	{
		    		"CidrIp" => Ref(:BastionHostCidr),
		    		"FromPort" => 22,
		    		"ToPort" => 22,
		    		"IpProtocol" => "tcp"
		    	}
		    ])
		        
		    Property("SecurityGroupEgress",[
		    	{
		    		"CidrIp" => Ref(:LogManagementCidr),
		    		"FromPort" => 5000,
		    		"ToPort" => 5000,
		    		"IpProtocol" => "tcp"
		    	}
		    ])

		    Property("SecurityGroupEgress",[
		    	{
		    		"CidrIp" => Ref(:PulpHostCidr),
		    		"FromPort" => 443,
		    		"ToPort" => 443,
		    		"IpProtocol" => "tcp"
		    	}
		    ])
		}

		# Outputs
		Output(:"#{e}VpcId") {
		    Description "VpcId of the #{e}Vpc"
		    Value Ref(:"#{e}Vpc")
		}

		Output(:"Mgmt#{e}PeeringCx") {
    		Description "Mgmt <-> #{e} Peering Connection Id"
        	Value Ref(:"Mgmt#{e}Peering")
    	}

    	Output(:"#{e}DBSubnetGroupId") {
    		Description "Id for DBSubnetGroup #{e}"
    		Value Ref(:"#{e}DBSubnetGroup")
    	}

    	Output(:"#{e}MgmtSecurityGroupId") {
    		Description "Management SG for #{e} VPC"
    		Value Ref(:"#{e}MgmtSecurityGroup")
    	}

    	zone_refs = []
    	net_tiers.each { |p|
    		zones.each { |z|
    			zone_refs << Ref(:"#{e}#{p}Subnet#{z}")
    		}
    		Output(:"#{e}#{p}SubnetIds") {
    			Description "List of #{p} subnet ids for #{e}"
    			Value FnJoin(",", zone_refs)
    		}
    	}
	}
}