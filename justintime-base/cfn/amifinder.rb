#!/usr/bin/env ruby
require 'aws-sdk'
require 'awesome_print'

# default region has no effect on outcome
ec2 = Aws::EC2::Client.new(:region => "eu-central-1")
imagename = "amzn-ami-vpc-nat-hvm-2015.09.1.x86_64-ebs"
owner = "137112412989"

type = "HVM64"
ids = {}

ec2.describe_regions.regions.each do |r|
	next if r.region_name == "us-gov-west-1" or r.region_name == "cn-north-1"
	ec2_img = Aws::EC2::Client.new(:region => r.region_name)
	img_result = ec2_img.describe_images(
		:owners => [ owner ],
		:filters => [ 
			{
				:name => "name",
				:values => [ imagename ]
			}
		]
	)
	if img_result
		ids[r.region_name] = { type => img_result.images[0].image_id }
	end
end

ap ids