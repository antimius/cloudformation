/* Security group for the nat server */
resource "aws_security_group" "natSG" {
  name = "natsg"
  description = "Security group for nat instances that allows SSH and VPN traffic from internet. Also allows outbound HTTP[S]"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  egress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags { 
    Name = "natsg" 
  }
}

resource "aws_security_group" "customerMgmtSG" {
  name = "customerMgmtSG"
  description = "customerMgtmSG"
  vpc_id = "${aws_vpc.default.id}"

  tags { 
    Name = "customerMgmtSG" 
  }
}

resource "aws_security_group" "ansibleSG" {
  name = "ansibleSG"
  description = "ansibleSG"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    security_groups = ["${aws_security_group.customerMgmtSG.id}"]
  }

  ingress {
    from_port = 3389
    to_port   = 3389
    protocol  = "tcp"
    security_groups = ["${aws_security_group.customerMgmtSG.id}"]
  }

  ingress {
    from_port = 5985
    to_port = 5986
    protocol = "tcp"
    security_groups = ["${aws_security_group.customerMgmtSG.id}"]
  }
 
  tags { 
    Name = "ansibleSG" 
  }
}

resource "aws_security_group" "gitpublicSG" {
  name = "gitpublicSG"
  description = "gitpublicSG"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["${var.ssh_location}"]
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = ["${var.ssh_location}"]
  }
  
  tags { 
    Name = "gitpublicSG" 
  }
}

resource "aws_security_group" "gitcoreSG" {
  name = "gitcoreSG"
  description = "gitcoreSG"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    security_groups = ["${aws_security_group.gitpublicSG.id}"]
  }
  
  tags { 
    Name = "gitcoreSG" 
  }
}

resource "aws_security_group" "jumphostSG" {
  name = "jumphostSG"
  description = "jumphostSG"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["${var.ssh_location}"]
  }
  
  tags { 
    Name = "jumphostSG" 
  }
}

resource "aws_security_group" "mgmtSG" {
  name = "mgmtSG"
  description = "mgmtSG"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    security_groups = ["${aws_security_group.jumphostSG.id}"]
  }

  ingress {
    from_port = 3389
    to_port   = 3389
    protocol  = "tcp"
    security_groups = ["${aws_security_group.customerMgmtSG.id}"]
  }
  
  tags { 
    Name = "mgmtSG" 
  }
}

