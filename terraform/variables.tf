variable "access_key" { 
  description = "AWS access key"
}

variable "secret_key" { 
  description = "AWS secret access key"
}

variable "region"     { 
  description = "AWS region to host your network"
  default     = "eu-central-1" 
}

variable "ssh_location" {
  description = "Location to allow SSH connections from"
  default = "213.214.96.4/32"
}

variable "vpc_cidr" {
  description = "CIDR for VPC"
  default     = "10.142.0.0/16"
}

variable "dmz_subnet_cidr" {
  description = "CIDR for public subnet"
  default     = "10.142.1.0/24"
}

variable "gitcore_subnet_cidr" {
  description = "CIDR for private subnet"
  default     = "10.142.2.0/24"
}

variable "management_subnet_cidr" {
  description = "CIDR for private subnet"
  default     = "10.142.3.0/24"
}

variable "client1_subnet_cidr" {
  description = "CIDR for private subnet"
  default     = "10.142.10.0/24"
}

variable "client2_subnet_cidr" {
  description = "CIDR for private subnet"
  default     = "10.142.20.0/24"
}

/* CentOS7 AMIs */
variable "centos7amis" {
  description = "CentOS7 AMIs"
  default = {
    us-east-1 = "ami-67a818fe" 
    us-west-2 = "ami-c7d092f7"
    us-west-1 = "ami-6bcfc42e"
    eu-central-1 = "ami-7cc4f661"
    eu-west-1 = "ami-e4ff5c93"
    ap-southeast-1 = "ami-aea582fc"
    ap-southeast-2 = "ami-bd523087"
    ap-northeast-1 = "ami-89634988"
    sa-east-1 = "ami-bf9520a2"
  }
}

/* Windows 2012 AMIs */
variable "win2012amis" {
  description = "Windows 2012 AMIs"
  default = {
    us-east-1 = "ami-cc93a8a4" 
    us-west-2 = "ami-8fd3f9bf"
    us-west-1 = "ami-6502e021"
    eu-central-1 = "ami-18e9d505"
    eu-west-1 = "ami-032b4b74"
    ap-southeast-1 = "ami-ae7c41fc"
    ap-southeast-2 = "ami-a9077a93"
    ap-northeast-1 = "ami-9641bb96"
    sa-east-1 = "ami-559b1e48"
  }
}

/* NAT instance AMIs */
variable "natamis" {
  description = "NAT AMIs"
  default = {
    us-east-1 = "ami-184dc970"
    us-west-2 = "ami-a98396ec"
    us-west-1 = "ami-290f4119"
    eu-west-1 = "ami-14913f63"
    eu-central-1 = "ami-ae380eb3"
    ap-southeast-1 = "ami-6aa38238"
    ap-southeast-2 = "ami-893f53b3"
    ap-northeast-1 = "ami-27d6e626"
    sa-east-1 = "ami-8122969c"
  }
}

