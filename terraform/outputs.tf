output "jumphost.ip" {
  value = "${aws_instance.jumphost.public_ip}"
}