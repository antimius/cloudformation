/* Internet gateway for the public subnet */
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
}

/* DMZ subnet */
resource "aws_subnet" "dmz" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "${var.dmz_subnet_cidr}"
  availability_zone = "eu-central-1a"
  map_public_ip_on_launch = true
  depends_on = ["aws_internet_gateway.default"]
  tags { 
    Name = "dmz" 
  }
}

/* Routing table for dmz subnet */
resource "aws_route_table" "dmz_route_table" {
  vpc_id = "${aws_vpc.default.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
  }
}

/* Associate the routing table to public subnet */
resource "aws_route_table_association" "dmz" {
  subnet_id = "${aws_subnet.dmz.id}"
  route_table_id = "${aws_route_table.dmz_route_table.id}"
}

/* Private subnets */
resource "aws_subnet" "gitcore" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "${var.gitcore_subnet_cidr}"
  availability_zone = "eu-central-1a"
  map_public_ip_on_launch = false
  depends_on = ["aws_internet_gateway.default"]
  tags { 
    Name = "gitcore" 
  }
}

resource "aws_subnet" "management" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "${var.management_subnet_cidr}"
  availability_zone = "eu-central-1b"
  map_public_ip_on_launch = false
  depends_on = ["aws_internet_gateway.default"]
  tags { 
    Name = "management" 
  }
}

resource "aws_subnet" "client1" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "${var.client1_subnet_cidr}"
  availability_zone = "eu-central-1b"
  map_public_ip_on_launch = false
  depends_on = ["aws_internet_gateway.default"]
  tags { 
    Name = "client1" 
  }
}

resource "aws_subnet" "client2" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "${var.client2_subnet_cidr}"
  availability_zone = "eu-central-1b"
  map_public_ip_on_launch = false
  depends_on = ["aws_internet_gateway.default"]
  tags { 
    Name = "client2" 
  }
}

/* Routing table for private subnets */
resource "aws_route_table" "private_route_table" {
  vpc_id = "${aws_vpc.default.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_instance.nat.id}"
  }
}

/* Routing table associations for private subnets */
resource "aws_route_table_association" "gitcore" {
  subnet_id = "${aws_subnet.gitcore.id}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}

resource "aws_route_table_association" "management" {
  subnet_id = "${aws_subnet.management.id}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}

resource "aws_route_table_association" "client1" {
  subnet_id = "${aws_subnet.client1.id}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}

resource "aws_route_table_association" "client2" {
  subnet_id = "${aws_subnet.client2.id}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}

