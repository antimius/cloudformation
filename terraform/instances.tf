/* NAT/VPN server */
resource "aws_instance" "nat" {
  ami = "${lookup(var.natamis, var.region)}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.dmz.id}"
  security_groups = ["${aws_security_group.natSG.id}", "${aws_security_group.mgmtSG.id}"]
  key_name = "${aws_key_pair.deployer.key_name}"
  source_dest_check = false
  tags = { 
    Name = "nat"
  }
}

resource "aws_instance" "jumphost" {
  ami = "${lookup(var.centos7amis, var.region)}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.dmz.id}"
  security_groups = ["${aws_security_group.customerMgmtSG.id}", "${aws_security_group.jumphostSG.id}"]
  key_name = "${aws_key_pair.deployer.key_name}"
  source_dest_check = false
  tags = { 
    Name = "jumphost"
  }
}

resource "aws_instance" "gitro" {
  ami = "${lookup(var.centos7amis, var.region)}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.dmz.id}"
  security_groups = ["${aws_security_group.gitpublicSG.id}", "${aws_security_group.mgmtSG.id}"]
  key_name = "${aws_key_pair.deployer.key_name}"
  source_dest_check = false
  tags = { 
    Name = "gitro"
  }
}

resource "aws_instance" "gitcore" {
  ami = "${lookup(var.centos7amis, var.region)}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.gitcore.id}"
  security_groups = ["${aws_security_group.gitcoreSG.id}", "${aws_security_group.mgmtSG.id}"]
  key_name = "${aws_key_pair.deployer.key_name}"
  source_dest_check = false
  tags = { 
    Name = "gitcore"
  }
}

resource "aws_instance" "management" {
  ami = "${lookup(var.centos7amis, var.region)}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.management.id}"
  security_groups = ["${aws_security_group.customerMgmtSG.id}", "${aws_security_group.mgmtSG.id}"]
  key_name = "${aws_key_pair.deployer.key_name}"
  source_dest_check = false
  tags = { 
    Name = "management"
  }
}

resource "aws_instance" "client1app1" {
  ami = "${lookup(var.centos7amis, var.region)}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.client1.id}"
  security_groups = ["${aws_security_group.ansibleSG.id}"]
  key_name = "${aws_key_pair.deployer.key_name}"
  source_dest_check = false
  tags = { 
    Name = "client1app1"
  }
}

resource "aws_instance" "client1app2" {
  ami = "${lookup(var.win2012amis, var.region)}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.dmz.id}"
  security_groups = ["${aws_security_group.ansibleSG.id}"]
  key_name = "${aws_key_pair.deployer.key_name}"
  source_dest_check = false
  tags = { 
    Name = "jumphost"
  }
}

resource "aws_instance" "client2app1" {
  ami = "${lookup(var.centos7amis, var.region)}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.dmz.id}"
  security_groups = ["${aws_security_group.ansibleSG.id}"]
  key_name = "${aws_key_pair.deployer.key_name}"
  source_dest_check = false
  tags = { 
    Name = "jumphost"
  }
}

resource "aws_instance" "client2app2" {
  ami = "${lookup(var.win2012amis, var.region)}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.dmz.id}"
  security_groups = ["${aws_security_group.ansibleSG.id}"]
  key_name = "${aws_key_pair.deployer.key_name}"
  source_dest_check = false
  tags = { 
    Name = "client2app2"
  }
}