#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Helper functions for creating CloudFormation templates through troposphere.
'''

from boto import ec2

def find_ami_mapping(imagename,owner,type="HVM64"):
    '''
    finds ami-ids for an image with a specific name in all regions
    it's slow af so better to use amifinder.py once
    '''
    ids = {}
    for region in ec2.regions():
        if(region.name == 'us-gov-west-1' or region.name == 'cn-north-1'): # skip gov and cn regions
            continue
        conn = ec2.connect_to_region(region.name)
        image = conn.get_all_images(owners=owner,filters={'name':imagename})[0] # only first ami listed is picked, too bad
        ids[region.name] = { type: image.id}
    return ids