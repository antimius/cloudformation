#!/usr/bin/env python
from boto import ec2

imagename = "RHEL-7.2_HVM_GA-20151112-x86_64-1-Hourly2-GP2"
owner = "309956199498" # RedHat
type = "HVM64"
ids = {}

for region in ec2.regions():
	if(region.name == 'us-gov-west-1' or region.name == 'cn-north-1'):
		continue
	conn = ec2.connect_to_region(region.name)
	image = conn.get_all_images(owners=owner,filters={'name':imagename})[0] # only first ami listed is picked, too bad
	ids[region.name] = { type: str(image.id)}

print ids	