#!/usr/bin/env python
# -*- coding: utf-8 -*-

from troposphere import Base64, FindInMap, GetAtt, Join, Output, GetAZs
from troposphere import Parameter, Ref, Tags, Template, Select
from troposphere.ec2 import Route, \
    VPCGatewayAttachment, SubnetRouteTableAssociation, Subnet, RouteTable, \
    VPC, EIP, Instance, InternetGateway, \
    SecurityGroupRule, SecurityGroup, SecurityGroupIngress
from troposphere.rds import DBInstance, DBSubnetGroup, DBSecurityGroup

t = Template()

t.add_version('2010-09-09')
t.add_description("Nginx Pagespeed FPM CloudFormation template\nJustin van Heerde @ Sentia 2015")

# parameters
keyname_param = t.add_parameter(
	Parameter(
		'KeyName',
		ConstraintDescription='Must be the name of an existing EC2 KeyPair.',
		Description='Name of an existing EC2 KeyPair to enable SSH access to the instance',
		Type='AWS::EC2::KeyPair::KeyName',
	))

dbname_param = t.add_parameter(
    Parameter(
        'DBName',
        Default="PrototypeDB",
        Description="The database name",
        Type="String",
        MinLength="1",
        MaxLength="64",
        AllowedPattern="[a-zA-Z][a-zA-Z0-9]*",
        ConstraintDescription=("must begin with a letter and contain only"
                           " alphanumeric characters.")
))

dbuser_param = t.add_parameter(
    Parameter(
        "DBUser",
        Description="The database admin account username",
        Type="String",
        MinLength="1",
        MaxLength="16",
        AllowedPattern="[a-zA-Z][a-zA-Z0-9]*",
        ConstraintDescription=("must begin with a letter and contain only"
                               " alphanumeric characters.")
))

dbpassword_param = t.add_parameter(
    Parameter(
        "DBPassword",
        NoEcho=True,
        Description="The database admin account password",
        Type="String",
        MinLength="1",
        MaxLength="41",
        AllowedPattern="[a-zA-Z0-9]*",
        ConstraintDescription="must contain only alphanumeric characters."
))

dbclass_param = t.add_parameter(
    Parameter(
        "DBClass",
        Default="db.t2.micro",
        Description="Database instance class",
        Type="String",
        AllowedValues=[
            "db.t2.micro", "db.t2.small", "db.t2.medium", "db.t2.large",
            "db.m4.large", "db.m4.xlarge", "db.m4.2xlarge", "db.m4.4xlarge", "db.m4.10xlarge",
            "db.m3.medium", "db.m3.large", "db.m3.xlarge", "db.m3.2xlarge",
            "db.r3.large", "db.r3.xlarge", "db.r3.2xlarge", "db.r3.4xlarge", "rb.r3.8xlarge"],
        ConstraintDescription="must select a valid database instance type.",
))

dballocatedstorage_param = t.add_parameter(
    Parameter(
        "DBAllocatedStorage",
        Default="5",
        Description="The size of the database (Gb)",
        Type="Number",
        MinValue="5",
        MaxValue="1024",
        ConstraintDescription="must be between 5 and 1024Gb.",
))

# Only allow HVM instance types
webInstanceType_param = t.add_parameter(
    Parameter(
        'webInstanceType',
        Type='String',
        Description='Web server EC2 instance type',
        Default='t2.micro',
        AllowedValues=[
            't2.micro', 't2.small', 't2.medium',
            'm3.medium', 'm3.large', 'm3.xlarge', 'm3.2xlarge',
            'c3.large', 'c3.xlarge', 'c3.2xlarge', 'c3.4xlarge', 'c3.8xlarge',
            'r3.large', 'r3.xlarge', 'r3.2xlarge', 'r3.4xlarge', 'r3.8xlarge',
            'i2.xlarge', 'i2.2xlarge', 'i2.4xlarge', 'i2.8xlarge',
            'hi1.4xlarge',
            'hs1.8xlarge',
            'cr1.8xlarge',
            'cc2.8xlarge',
        ],
        ConstraintDescription='must be a valid EC2 instance type.',
    ))

# mappings
t.add_mapping('AWSInstanceType2Arch', {
    't1.micro': {'Arch': 'PV64'}, 't2.micro': {'Arch': 'HVM64'}, 't2.small': {'Arch': 'HVM64'},
    't2.medium': {'Arch': 'HVM64'}, 'm1.small': {'Arch': 'PV64'}, 'm1.medium': {'Arch': 'PV64'},
    'm1.large': {'Arch': 'PV64'}, 'm1.xlarge': {'Arch': 'PV64'}, 'm2.xlarge': {'Arch': 'PV64'},
    'm2.2xlarge': {'Arch': 'PV64'}, 'm2.4xlarge': {'Arch': 'PV64'}, 'm3.medium': {'Arch': 'HVM64'},
    'm3.large': {'Arch': 'HVM64'}, 'm3.xlarge': {'Arch': 'HVM64'}, 'm3.2xlarge': {'Arch': 'HVM64'},
    'c1.medium': {'Arch': 'PV64'}, 'c1.xlarge': {'Arch': 'PV64'}, 'c3.large': {'Arch': 'HVM64'},
    'c3.xlarge': {'Arch': 'HVM64'}, 'c3.2xlarge': {'Arch': 'HVM64'}, 'c3.4xlarge': {'Arch': 'HVM64'},
    'c3.8xlarge': {'Arch': 'HVM64'}, 'g2.2xlarge': {'Arch': 'HVMG2'},'r3.large': {'Arch': 'HVM64'},
    'r3.xlarge': {'Arch': 'HVM64'}, 'r3.2xlarge': {'Arch': 'HVM64'}, 'r3.4xlarge': {'Arch': 'HVM64'},
    'r3.8xlarge': {'Arch': 'HVM64'}, 'i2.xlarge': {'Arch': 'HVM64'}, 'i2.2xlarge': {'Arch': 'HVM64'},
    'i2.4xlarge': {'Arch': 'HVM64'}, 'i2.8xlarge': {'Arch': 'HVM64'}, 'hi1.4xlarge': {'Arch': 'HVM64'},
    'hs1.8xlarge': {'Arch': 'HVM64'}, 'cr1.8xlarge': {'Arch': 'HVM64'}, 'cc2.8xlarge': {'Arch': 'HVM64'},
})

# RedHat 7.2 AMIs
t.add_mapping('AWSRegionArch2Redhat72AMI', {
    'us-east-1': {'HVM64': 'ami-2051294a'},
    'ap-northeast-1': {'HVM64': 'ami-0dd8f963'},
    'sa-east-1': {'HVM64': 'ami-27b3094b'},
    'ap-southeast-1': {'HVM64': 'ami-3f03c55c'},
    'ap-southeast-2': {'HVM64': 'ami-e0c19f83'},
    'us-west-2': {'HVM64': 'ami-775e4f16'},
    'us-west-1': {'HVM64': 'ami-d1315fb1'},
    'eu-central-1': {'HVM64': 'ami-875042eb'},
    'eu-west-1': {'HVM64': 'ami-8b8c57f8'}})

# userdata for RedHat hosts
userdata_redhat = Base64(
            Join(
                '',
                [
                    "#!/bin/bash -v\n",
                    "yum update -y aws*\n",
                    "\n"
                ])
)

# fetch region and stack data
ref_stack_id = Ref('AWS::StackId')
ref_region = Ref('AWS::Region')
ref_stack_name = Ref('AWS::StackName')

# create a new VPC
VPC = t.add_resource(VPC('PrototypeVPC', CidrBlock='172.16.0.0/16'))

# subnets
# make sure subnets are split between availability zones
protoSubnet = t.add_resource(Subnet('protoSubnet', CidrBlock='172.16.1.0/24', VpcId=Ref(VPC)))
protoSubnet2 = t.add_resource(Subnet('protoSubnet2', CidrBlock='172.16.2.0/24', VpcId=Ref(VPC)))
protoDBSubnetGroup = t.add_resource(DBSubnetGroup(
    "ProtoDBSubnetGroup",
    DBSubnetGroupDescription="Subnets available for the RDS DB Instance",
    SubnetIds=[Ref(protoSubnet), Ref(protoSubnet2)],
))

# internet gateway
internetGateway = t.add_resource(InternetGateway('InternetGateway'))
gatewayAttachment = t.add_resource(VPCGatewayAttachment('AttachGateway', VpcId=Ref(VPC),
    InternetGatewayId=Ref(internetGateway)))

# route tables
protoRouteTable = t.add_resource(RouteTable('protoRouteTable', VpcId=Ref(VPC)))

# route table associations
t.add_resource(SubnetRouteTableAssociation('protoSubnetRouteTableAssociation', SubnetId=Ref(protoSubnet),
    RouteTableId=Ref(protoRouteTable) ))

# security groups
publicSG = t.add_resource(
    SecurityGroup('publicSG',
    	GroupDescription="publicSG",
        SecurityGroupIngress=[
            SecurityGroupRule(IpProtocol='tcp', CidrIp='213.214.96.4/32', FromPort='22', ToPort='22'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='213.214.96.4/32', FromPort='80', ToPort='80'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='213.214.96.4/32', FromPort='443', ToPort='443'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.207.225.61/32', FromPort='22', ToPort='22'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.207.225.61/32', FromPort='80', ToPort='80'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.207.225.61/32', FromPort='443', ToPort='443'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.107.179.154/32', FromPort='22', ToPort='22'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.107.179.154/32', FromPort='80', ToPort='80'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.107.179.154/32', FromPort='443', ToPort='443'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='85.150.223.50/32', FromPort='22', ToPort='22'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='85.150.223.50/32', FromPort='80', ToPort='80'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='85.150.223.50/32', FromPort='443', ToPort='443'),
        ],
    	VpcId=Ref(VPC)
))

dbSG = t.add_resource(
    SecurityGroup('dbSG',
        GroupDescription="dbSG",
        SecurityGroupIngress=[
            SecurityGroupRule(IpProtocol='tcp', SourceSecurityGroupId=Ref(publicSG), FromPort='3306', ToPort='3306'),
        ],
        VpcId=Ref(VPC)
))

# instances
webInstance = t.add_resource(
	Instance(
        'webInstance',
        ImageId=FindInMap('AWSRegionArch2Redhat72AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(webInstanceType_param), 'Arch')), InstanceType=Ref(webInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(protoSubnet),
        SecurityGroupIds=[Ref(publicSG)],
        UserData=userdata_redhat,
))

# RDS instance
dbInstance = t.add_resource(
    DBInstance(
        "dbInstance",
        DBName=Ref(dbname_param),
        AllocatedStorage=Ref(dballocatedstorage_param),
        DBInstanceClass=Ref(dbclass_param),
        Engine="MySQL",
        EngineVersion="5.6",
        MasterUsername=Ref(dbuser_param),
        MasterUserPassword=Ref(dbpassword_param),
        DBSubnetGroupName=Ref(protoDBSubnetGroup),
        VPCSecurityGroups=[Ref(dbSG)],
        DeletionPolicy="Retain",
        AvailabilityZone=GetAtt(protoSubnet, "AvailabilityZone"),
))

# Elastic IPs
webEIP = t.add_resource(EIP('webEIP', DependsOn='AttachGateway', Domain='vpc', InstanceId=Ref(webInstance)))

# routes
t.add_resource(Route('publicRoute1', DependsOn='AttachGateway', GatewayId=Ref(internetGateway),
    DestinationCidrBlock='0.0.0.0/0', RouteTableId=Ref(protoRouteTable)))

# Outputs
t.add_output([Output('webEIP', Description='Web EIP', Value=Ref(webEIP))])
t.add_output([Output('webPublicIP', Description='Web instance public IP', Value=GetAtt(webInstance, "PublicIp"))])
t.add_output([Output('webPublicDnsName', Description='Web instance public DNS', Value=GetAtt(webInstance, "PublicDnsName"))])
t.add_output([Output('dbEndpointAddress', Description='DB instance endpoint IP', Value=GetAtt(dbInstance, "Endpoint.Address"))])
t.add_output([Output('dbEndpointPort', Description='DB instance endpoint port', Value=GetAtt(dbInstance, "Endpoint.Port"))])

print(t.to_json())
