#!/bin/sh
./nginx_template_generator.py > nginx-proto.template
aws s3 cp nginx-proto.template s3://justintest-cloudformation-templates
aws cloudformation validate-template --template-url https://s3.eu-central-1.amazonaws.com/justintest-cloudformation-templates/nginx-proto.template
echo https://s3.eu-central-1.amazonaws.com/justintest-cloudformation-templates/nginx-proto.template
