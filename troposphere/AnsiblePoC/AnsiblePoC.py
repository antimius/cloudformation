#!/usr/bin/env python
# -*- coding: utf-8 -*-

from troposphere import Base64, FindInMap, GetAtt, Join, Output, GetAZs
from troposphere import Parameter, Ref, Tags, Template, Select
from troposphere.autoscaling import Metadata
from troposphere.ec2 import PortRange, NetworkAcl, Route, \
    VPCGatewayAttachment, SubnetRouteTableAssociation, Subnet, RouteTable, \
    VPC, NetworkInterfaceProperty, NetworkAclEntry, \
    SubnetNetworkAclAssociation, EIP, Instance, InternetGateway, \
    SecurityGroupRule, SecurityGroup, SecurityGroupIngress
from troposphere.policies import CreationPolicy, ResourceSignal
from troposphere.cloudformation import Init, InitFile, InitFiles, \
    InitConfig, InitService, InitServices

t = Template()

t.add_version('2010-09-09')
t.add_description("Ansible PoC CloudFormation template (Simplified)\nJustin van Heerde @ Sentia 2015")

keyname_param = t.add_parameter(
	Parameter(
		'KeyName',
		ConstraintDescription='Must be the name of an existing EC2 KeyPair.',
		Description='Name of an existing EC2 KeyPair to enable SSH access to the instance',
		Type='AWS::EC2::KeyPair::KeyName',
	))

# Only allow HVM instance types
mgmtInstanceType_param = t.add_parameter(Parameter(
	'mgmtInstanceType',
	Type='String',
	Description='Management server EC2 instance type',
	Default='t2.medium',
	AllowedValues=[
		't2.micro', 't2.small', 't2.medium',
		'm3.medium', 'm3.large', 'm3.xlarge', 'm3.2xlarge',
		'c3.large', 'c3.xlarge', 'c3.2xlarge', 'c3.4xlarge', 'c3.8xlarge',
		'r3.large', 'r3.xlarge', 'r3.2xlarge', 'r3.4xlarge', 'r3.8xlarge',
		'i2.xlarge', 'i2.2xlarge', 'i2.4xlarge', 'i2.8xlarge',
		'hi1.4xlarge',
		'hs1.8xlarge',
		'cr1.8xlarge',
		'cc2.8xlarge',
	],
	ConstraintDescription='must be a valid EC2 instance type.',
	))

windowsInstanceType_param = t.add_parameter(Parameter(
    'windowsInstanceType',
    Type='String',
    Description='Customer Windows server EC2 instance type',
    Default='t2.medium',
    AllowedValues=[
        't2.micro', 't2.small', 't2.medium',
        'm3.medium', 'm3.large', 'm3.xlarge', 'm3.2xlarge',
        'c3.large', 'c3.xlarge', 'c3.2xlarge', 'c3.4xlarge', 'c3.8xlarge',
        'r3.large', 'r3.xlarge', 'r3.2xlarge', 'r3.4xlarge', 'r3.8xlarge',
        'i2.xlarge', 'i2.2xlarge', 'i2.4xlarge', 'i2.8xlarge',
        'hi1.4xlarge',
        'hs1.8xlarge',
        'cr1.8xlarge',
        'cc2.8xlarge',
    ],
    ConstraintDescription='must be a valid EC2 instance type.',
    ))

linuxInstanceType_param = t.add_parameter(Parameter(
    'linuxInstanceType',
    Type='String',
    Description='Customer Linux server EC2 instance type',
    Default='t2.medium',
    AllowedValues=[
        't2.micro', 't2.small', 't2.medium',
        'm3.medium', 'm3.large', 'm3.xlarge', 'm3.2xlarge',
        'c3.large', 'c3.xlarge', 'c3.2xlarge', 'c3.4xlarge', 'c3.8xlarge',
        'r3.large', 'r3.xlarge', 'r3.2xlarge', 'r3.4xlarge', 'r3.8xlarge',
        'i2.xlarge', 'i2.2xlarge', 'i2.4xlarge', 'i2.8xlarge',
        'hi1.4xlarge',
        'hs1.8xlarge',
        'cr1.8xlarge',
        'cc2.8xlarge',
    ],
    ConstraintDescription='must be a valid EC2 instance type.',
    ))

t.add_mapping('AWSInstanceType2Arch', {
    't1.micro': {'Arch': 'PV64'},
    't2.micro': {'Arch': 'HVM64'},
    't2.small': {'Arch': 'HVM64'},
    't2.medium': {'Arch': 'HVM64'},
    'm1.small': {'Arch': 'PV64'},
    'm1.medium': {'Arch': 'PV64'},
    'm1.large': {'Arch': 'PV64'},
    'm1.xlarge': {'Arch': 'PV64'},
    'm2.xlarge': {'Arch': 'PV64'},
    'm2.2xlarge': {'Arch': 'PV64'},
    'm2.4xlarge': {'Arch': 'PV64'},
    'm3.medium': {'Arch': 'HVM64'},
    'm3.large': {'Arch': 'HVM64'},
    'm3.xlarge': {'Arch': 'HVM64'},
    'm3.2xlarge': {'Arch': 'HVM64'},
    'c1.medium': {'Arch': 'PV64'},
    'c1.xlarge': {'Arch': 'PV64'},
    'c3.large': {'Arch': 'HVM64'},
    'c3.xlarge': {'Arch': 'HVM64'},
    'c3.2xlarge': {'Arch': 'HVM64'},
    'c3.4xlarge': {'Arch': 'HVM64'},
    'c3.8xlarge': {'Arch': 'HVM64'},
    'g2.2xlarge': {'Arch': 'HVMG2'},
    'r3.large': {'Arch': 'HVM64'},
    'r3.xlarge': {'Arch': 'HVM64'},
    'r3.2xlarge': {'Arch': 'HVM64'},
    'r3.4xlarge': {'Arch': 'HVM64'},
    'r3.8xlarge': {'Arch': 'HVM64'},
    'i2.xlarge': {'Arch': 'HVM64'},
    'i2.2xlarge': {'Arch': 'HVM64'},
    'i2.4xlarge': {'Arch': 'HVM64'},
    'i2.8xlarge': {'Arch': 'HVM64'},
    'hi1.4xlarge': {'Arch': 'HVM64'},
    'hs1.8xlarge': {'Arch': 'HVM64'},
    'cr1.8xlarge': {'Arch': 'HVM64'},
    'cc2.8xlarge': {'Arch': 'HVM64'},
})

# CentOS7 AMIs
t.add_mapping('AWSRegionArch2CentOS7AMI', {
    'us-east-1': { 'HVM64': 'ami-96a818fe' },
    'us-west-2': { 'HVM64': 'ami-c7d092f7' },
    'us-west-1': { 'HVM64': 'ami-6bcfc42e' },
    'eu-west-1': { 'HVM64': 'ami-e4ff5c93' },
    'eu-central-1': { 'HVM64': 'ami-7cc4f661'},
    'ap-southeast-1': { 'HVM64': 'ami-aea582fc' },
    'ap-southeast-2': { 'HVM64': 'ami-bd523087' },
    'ap-northeast-1': { 'HVM64': 'ami-89634988' },
    'sa-east-1': { 'HVM64': 'ami-bf9520a2' },
})

# Windows 2012 AMIs
t.add_mapping('AWSRegionArch2Win2012AMI', {
	'us-east-1': { 'HVM64': 'ami-5b9e6b30' },
    'us-west-2': { 'HVM64': 'ami-67c7ff57' },
    'us-west-1': { 'HVM64': 'ami-c5688281' },
    'eu-central-1': { 'HVM64': 'ami-2e87be33'},
    'eu-west-1': { 'HVM64': 'ami-c1740ab6' },
    'ap-southeast-1': { 'HVM64': 'ami-e0d1ebb2' },
    'ap-southeast-2': { 'HVM64': 'ami-f9760dc3' },
    'ap-northeast-1': { 'HVM64': 'ami-c6b46dc6' },
    'sa-east-1': { 'HVM64': 'ami-b93bbba4' },
})

# Windows 2008 AMIs
t.add_mapping('AWSRegionArch2Win2008AMI', {
    'us-east-1': { 'HVM64': 'ami-5fe81d34' },
    'us-west-2': { 'HVM64': 'ami-73b08843' },
    'us-west-1': { 'HVM64': 'ami-0d6d8749' },
    'eu-central-1': { 'HVM64': 'ami-6278407f'},
    'eu-west-1': { 'HVM64': 'ami-43691734' },
    'ap-southeast-1': { 'HVM64': 'ami-c2d5ef90' },
    'ap-southeast-2': { 'HVM64': 'ami-3168130b' },
    'ap-northeast-1': { 'HVM64': 'ami-70a17870' },
    'sa-east-1': { 'HVM64': 'ami-cfcf4cd2' },

})

# CentOS 6.5 AMIs
t.add_mapping('AWSRegionArch2CentOS65AMI', {
    'us-east-1': { 'HVM64': 'ami-8997afe0' },
    'us-west-2': { 'HVM64': 'ami-b6bdde86' },
    'us-west-1': { 'HVM64': 'ami-1a013c5f' },
    'eu-central-1': { 'HVM64': 'ami-98a79585'},
    'eu-west-1': { 'HVM64': 'ami-42718735' },
    'ap-southeast-1': { 'HVM64': 'ami-a08fd9f2' },
    'ap-southeast-2': { 'HVM64': 'ami-e7138ddd' },
    'ap-northeast-1': { 'HVM64': 'ami-81294380' },
    'sa-east-1': { 'HVM64': 'ami-7d02a260' },
})

# Ubuntu 14.04 LTS
t.add_mapping('AWSRegionArch2Ubuntu1404LTSAMI', {
    'us-east-1': { 'HVM64': 'ami-2dcf7b46' },
    'us-west-2': { 'HVM64': 'ami-97d5c0a7' },
    'us-west-1': { 'HVM64': 'ami-976d93d3' },
    'eu-central-1': { 'HVM64': 'ami-8ae0e797'},
    'eu-west-1': { 'HVM64': 'ami-92401ce5' },
    'ap-southeast-1': { 'HVM64': 'ami-72353b20' },
    'ap-southeast-2': { 'HVM64': 'ami-3d054707' },
    'ap-northeast-1': { 'HVM64': 'ami-b405bfb4' },
    'sa-east-1': { 'HVM64': 'ami-d1a129cc' },
    })

# NAT instance mapping
t.add_mapping('NatRegionMap', {
	"us-east-1": { "AMI": "ami-184dc970" },
	"us-west-1": { "AMI": "ami-a98396ec" },
	"us-west-2": { "AMI": "ami-290f4119" },
    "eu-central-1": { "AMI": "ami-ae380eb3" },
	"eu-west-1": { "AMI": "ami-14913f63" },
	"sa-east-1": { "AMI": "ami-8122969c" },
	"ap-southeast-1": { "AMI": "ami-6aa38238" },
	"ap-southeast-2": { "AMI": "ami-893f53b3" },
	"ap-northeast-1": { "AMI": "ami-27d6e626" },
})

# userdata for centos hosts
userdata_centos = Base64(
            Join(
                '',
                [
                    "#!/bin/bash -v\n",
                    "yum update -y aws*\n",         
                    "\n"
                ])
)
# userdata for windows hosts
userdata_windows = Base64(
            Join(
                '',
                [
                    "<powershell>",
                    "Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force\n",
                    "$storageDir = $pwd\n",
                    "$webclient = New-Object System.Net.WebClient\n",
                    "$url = \"https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1\"\n",
                    "$file = \"C:\\Program Files\\Amazon\\Ec2ConfigService\\Scripts\\ConfigureRemotingForAnsible.ps1\"\n",
                    "$webclient.DownloadFile($url,$file)\n",
                    "$VerbosePreference=\"Continue\"\n",
                    "& $file\n",
                    "</powershell>"])
)
# userdata for ubuntu hosts
userdata_ubuntu = Base64(
            Join(
                '',
                [
                    "#!/bin/bash -v\n",
                    "export DEBIAN_FRONTEND=noninteractive\n",
                    "apt-get update\n",
                    "apt-get dist-upgrade -y\n",         
                    "\n"
                ])
)

# fetch region and stack data
ref_stack_id = Ref('AWS::StackId')
ref_region = Ref('AWS::Region')
ref_stack_name = Ref('AWS::StackName')

# create a new VPC
VPC = t.add_resource(VPC('AnsiblePoCVPC', CidrBlock='10.0.0.0/16', Tags=Tags(Application=ref_stack_id)))

# subnets
# make sure subnets are split between availability zones
dmzSubnet = t.add_resource(Subnet('dmzSubnet', CidrBlock='10.0.1.0/24', VpcId=Ref(VPC), AvailabilityZone=Select(0, GetAZs("")), Tags=Tags(Application=ref_stack_id)))
internalSubnet = t.add_resource(Subnet('internalSubnet', CidrBlock='10.0.2.0/24', VpcId=Ref(VPC), AvailabilityZone=Select(0, GetAZs("")), Tags=Tags(Application=ref_stack_id)))

# internet gateway
internetGateway = t.add_resource(InternetGateway('InternetGateway', Tags=Tags(Application=ref_stack_id)))
gatewayAttachment = t.add_resource(VPCGatewayAttachment('AttachGateway', VpcId=Ref(VPC), InternetGatewayId=Ref(internetGateway)))

# route tables
dmzRouteTable = t.add_resource(RouteTable('dmzRouteTable', VpcId=Ref(VPC), Tags=Tags(Application=ref_stack_id)))
internalRouteTable = t.add_resource(RouteTable('internalRouteTable', VpcId=Ref(VPC), Tags=Tags(Application=ref_stack_id)))

# route table associations
t.add_resource(SubnetRouteTableAssociation('dmzSubnetRouteTableAssociation', SubnetId=Ref(dmzSubnet), RouteTableId=Ref(dmzRouteTable), ))
t.add_resource(SubnetRouteTableAssociation('internalSubnetRouteTableAssociation', SubnetId=Ref(internalSubnet), RouteTableId=Ref(internalRouteTable), ))

# security groups
publicSG = t.add_resource(
    SecurityGroup('publicSG',
    	GroupDescription="publicSG",
        SecurityGroupIngress=[
            SecurityGroupRule(IpProtocol='tcp', CidrIp='213.214.96.4/32', FromPort='22', ToPort='22'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='213.214.96.4/32', FromPort='80', ToPort='80'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='213.214.96.4/32', FromPort='443', ToPort='443'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.207.225.61/32', FromPort='22', ToPort='22'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.207.225.61/32', FromPort='80', ToPort='80'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.207.225.61/32', FromPort='443', ToPort='443'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.107.179.154/32', FromPort='22', ToPort='22'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.107.179.154/32', FromPort='80', ToPort='80'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='84.107.179.154/32', FromPort='443', ToPort='443'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='85.150.223.50/32', FromPort='22', ToPort='22'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='85.150.223.50/32', FromPort='80', ToPort='80'),
            SecurityGroupRule(IpProtocol='tcp', CidrIp='85.150.223.50/32', FromPort='443', ToPort='443'),
        ],
    	VpcId=Ref(VPC)
))

clientMgmtSG = t.add_resource(
    SecurityGroup('clientMgmtSG',
    	GroupDescription="clientMgmtSG",
    	SecurityGroupIngress=[
    		SecurityGroupRule(IpProtocol='tcp', SourceSecurityGroupId=Ref(publicSG), FromPort='22', ToPort='22'),
    		SecurityGroupRule(IpProtocol='tcp', SourceSecurityGroupId=Ref(publicSG), FromPort='3389', ToPort='3389'),
            SecurityGroupRule(IpProtocol='tcp', SourceSecurityGroupId=Ref(publicSG), FromPort='5985', ToPort='5986')],
    	VpcId=Ref(VPC)
))

c2pIngress = t.add_resource(
    SecurityGroupIngress('c2pIngress',
        GroupId=Ref(publicSG), IpProtocol='tcp', SourceSecurityGroupId=Ref(clientMgmtSG), FromPort='22', ToPort='22')
)

NATSG = t.add_resource(
    SecurityGroup('NATSG',
    	GroupDescription="NATSG",
    	SecurityGroupIngress=[
    		SecurityGroupRule(IpProtocol='-1', CidrIp='10.0.0.0/16', FromPort='0', ToPort='65535'),
    		SecurityGroupRule(IpProtocol='-1', CidrIp='10.0.0.0/16', FromPort='0', ToPort='65535'),
            SecurityGroupRule(IpProtocol='tcp', SourceSecurityGroupId=Ref(publicSG), FromPort='22', ToPort='22'),
    	],
    	SecurityGroupEgress=[
    		SecurityGroupRule(IpProtocol='-1', CidrIp='0.0.0.0/0', FromPort='0', ToPort='65535'),
    		SecurityGroupRule(IpProtocol='-1', CidrIp='0.0.0.0/0', FromPort='0', ToPort='65535'),
    	],
    	VpcId=Ref(VPC)
))

# instances
natInstance = t.add_resource(
    Instance(
        'natInstance',
        ImageId=FindInMap('NatRegionMap', Ref('AWS::Region'), 'AMI'), InstanceType='t2.micro',
        KeyName=Ref(keyname_param),
        SourceDestCheck=False,
        SubnetId=Ref(dmzSubnet),
        SecurityGroupIds=[Ref(NATSG)],
        UserData=Base64(
            Join(
                '',
                [
                    "#!/bin/bash -v\n",
					"yum update -y aws*\n",
					
					"# Configure iptables\n",
					"/sbin/iptables -t nat -A POSTROUTING -o eth0 -s 0.0.0.0/0 -j MASQUERADE\n",
					"# Configure ip forwarding and redirects\n",
					"echo 1 >  /proc/sys/net/ipv4/ip_forward && echo 0 >  /proc/sys/net/ipv4/conf/eth0/send_redirects\n",
					"mkdir -p /etc/sysctl.d/\n",
					"cat <<EOF > /etc/sysctl.d/nat.conf\n",
					"net.ipv4.ip_forward = 1\n",
					"net.ipv4.conf.eth0.send_redirects = 0\n",
					"EOF\n",
					"/sbin/iptables-save > /etc/sysconfig/iptables\n",
					"\n"
                ])),
        Tags=Tags(Application=ref_stack_id),
))

mgmtInstance = t.add_resource(
	Instance(
        'mgmtInstance',
        ImageId=FindInMap('AWSRegionArch2CentOS7AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(mgmtInstanceType_param), 'Arch')), InstanceType=Ref(mgmtInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(dmzSubnet),
        SecurityGroupIds=[Ref(publicSG)],
        UserData=userdata_centos,
        Tags=Tags(Application=ref_stack_id),
))

ansible1Instance = t.add_resource(
    Instance(
        'ansible1Instance',
        ImageId=FindInMap('AWSRegionArch2CentOS7AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(mgmtInstanceType_param), 'Arch')), InstanceType=Ref(mgmtInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(internalSubnet),
        SecurityGroupIds=[Ref(clientMgmtSG)],
        UserData=userdata_centos,
        Tags=Tags(Application=ref_stack_id),
))

ansible2Instance = t.add_resource(
    Instance(
        'ansible2Instance',
        ImageId=FindInMap('AWSRegionArch2CentOS7AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(mgmtInstanceType_param), 'Arch')), InstanceType=Ref(mgmtInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(internalSubnet),
        SecurityGroupIds=[Ref(clientMgmtSG)],
        UserData=userdata_centos,
        Tags=Tags(Application=ref_stack_id),
))

client1app1Instance = t.add_resource(
	Instance(
        'client1app1Instance',
        ImageId=FindInMap('AWSRegionArch2CentOS7AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(linuxInstanceType_param), 'Arch')), InstanceType=Ref(linuxInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(internalSubnet),
        SecurityGroupIds=[Ref(clientMgmtSG)],
        UserData=userdata_centos,
        Tags=Tags(Application=ref_stack_id),
))

client1app2Instance = t.add_resource(
	Instance(
        'client1app2Instance',
        ImageId=FindInMap('AWSRegionArch2Win2012AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(windowsInstanceType_param), 'Arch')), InstanceType=Ref(windowsInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(internalSubnet),
        SecurityGroupIds=[Ref(clientMgmtSG)],
        Tags=Tags(Application=ref_stack_id),
        UserData=userdata_windows
))

client1app3Instance = t.add_resource(
    Instance(
        'client1app3Instance',
        ImageId=FindInMap('AWSRegionArch2CentOS65AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(linuxInstanceType_param), 'Arch')), InstanceType=Ref(linuxInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(internalSubnet),
        SecurityGroupIds=[Ref(clientMgmtSG)],
        UserData=userdata_centos,
        Tags=Tags(Application=ref_stack_id),
))

client1app4Instance = t.add_resource(
    Instance(
        'client1app4Instance',
        ImageId=FindInMap('AWSRegionArch2Ubuntu1404LTSAMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(linuxInstanceType_param), 'Arch')), InstanceType=Ref(linuxInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(internalSubnet),
        SecurityGroupIds=[Ref(clientMgmtSG)],
        UserData=userdata_ubuntu,
        Tags=Tags(Application=ref_stack_id),
))

client2app1Instance = t.add_resource(
	Instance(
        'client2app1Instance',
        ImageId=FindInMap('AWSRegionArch2CentOS65AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(linuxInstanceType_param), 'Arch')), InstanceType=Ref(linuxInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(internalSubnet),
        SecurityGroupIds=[Ref(clientMgmtSG)],
        UserData=userdata_centos,
        Tags=Tags(Application=ref_stack_id),
))

client2app2Instance = t.add_resource(
	Instance(
        'client2app2Instance',
        ImageId=FindInMap('AWSRegionArch2Win2008AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(windowsInstanceType_param), 'Arch')), InstanceType=Ref(windowsInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(internalSubnet),
        SecurityGroupIds=[Ref(clientMgmtSG)],
        Tags=Tags(Application=ref_stack_id),
        UserData=userdata_windows,
))

client2app3Instance = t.add_resource(
    Instance(
        'client2app3Instance',
        ImageId=FindInMap('AWSRegionArch2CentOS65AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(linuxInstanceType_param), 'Arch')), InstanceType=Ref(linuxInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(internalSubnet),
        SecurityGroupIds=[Ref(clientMgmtSG)],
        UserData=userdata_centos,
        Tags=Tags(Application=ref_stack_id),
))

client2app4Instance = t.add_resource(
    Instance(
        'client2app4Instance',
        ImageId=FindInMap('AWSRegionArch2Ubuntu1404LTSAMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(linuxInstanceType_param), 'Arch')), InstanceType=Ref(linuxInstanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(internalSubnet),
        SecurityGroupIds=[Ref(clientMgmtSG)],
        UserData=userdata_ubuntu,
        Tags=Tags(Application=ref_stack_id),
))

# Elastic IPs
mgmtEIP = t.add_resource(EIP('mgmtEIP', DependsOn='AttachGateway', Domain='vpc', InstanceId=Ref(mgmtInstance)))
natEIP = t.add_resource(EIP('natEIP', DependsOn='AttachGateway', Domain='vpc', InstanceId=Ref(natInstance)))

# routes
t.add_resource(Route('dmzRoute1', DependsOn='AttachGateway', GatewayId=Ref(internetGateway), DestinationCidrBlock='0.0.0.0/0', RouteTableId=Ref(dmzRouteTable)))
t.add_resource(Route('internalRoute1', DependsOn='natInstance', InstanceId=Ref(natInstance), DestinationCidrBlock='0.0.0.0/0', RouteTableId=Ref(internalRouteTable)))

# Outputs 
t.add_output([Output('mgmtEIP', Description='Management EIP', Value=Ref(mgmtEIP))])
t.add_output([Output('natEIP', Description='NAT instance EIP', Value=Ref(natEIP))])
t.add_output([Output('natIP', Description='Nat instance private IP', Value=GetAtt(natInstance, "PrivateIp"))])
t.add_output([Output('mgmtIP', Description='Mgmt instance private IP', Value=GetAtt(mgmtInstance, "PrivateIp"))])
t.add_output([Output('ansible1IP', Description='Ansible instance 1 private IP', Value=GetAtt(ansible1Instance, "PrivateIp"))])
t.add_output([Output('ansible2IP', Description='Ansible instance 2 private IP', Value=GetAtt(ansible2Instance, "PrivateIp"))])
t.add_output([Output('client1app1IP', Description='Client1App1 (CentOS) instance private IP', Value=GetAtt(client1app1Instance, "PrivateIp"))])
t.add_output([Output('client1app2IP', Description='Client1App2 (Windows) instance private IP', Value=GetAtt(client1app2Instance, "PrivateIp"))])
t.add_output([Output('client1app3IP', Description='Client1App3 (CentOS) instance private IP', Value=GetAtt(client1app3Instance, "PrivateIp"))])
t.add_output([Output('client1app4IP', Description='Client1App4 (Ubuntu) instance private IP', Value=GetAtt(client1app4Instance, "PrivateIp"))])
t.add_output([Output('client2app1IP', Description='Client2App1 (CentOS) instance private IP', Value=GetAtt(client2app1Instance, "PrivateIp"))])
t.add_output([Output('client2app2IP', Description='Client2App2 (Windows) instance private IP', Value=GetAtt(client2app2Instance, "PrivateIp"))])
t.add_output([Output('client2app3IP', Description='Client2App3 (CentOS) instance private IP', Value=GetAtt(client2app3Instance, "PrivateIp"))])
t.add_output([Output('client2app4IP', Description='Client2App4 (Ubuntu) instance private IP', Value=GetAtt(client2app4Instance, "PrivateIp"))])

print(t.to_json())