#!/bin/sh
./AnsiblePoCSimple.py > anspoc.template
aws --profile l3 s3 cp anspoc.template s3://sentia-ansible-poc
aws --profile l3 cloudformation validate-template --template-url https://s3.eu-central-1.amazonaws.com/sentia-ansible-poc/anspoc.template
