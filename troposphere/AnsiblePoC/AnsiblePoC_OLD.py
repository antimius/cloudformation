#!/usr/bin/env python
# -*- coding: utf-8 -*-

from troposphere import Base64, FindInMap, GetAtt, Join, Output, GetAZs
from troposphere import Parameter, Ref, Tags, Template, Select
from troposphere.autoscaling import Metadata
from troposphere.ec2 import PortRange, NetworkAcl, Route, \
    VPCGatewayAttachment, SubnetRouteTableAssociation, Subnet, RouteTable, \
    VPC, NetworkInterfaceProperty, NetworkAclEntry, \
    SubnetNetworkAclAssociation, EIP, Instance, InternetGateway, \
    SecurityGroupRule, SecurityGroup
from troposphere.policies import CreationPolicy, ResourceSignal
from troposphere.cloudformation import Init, InitFile, InitFiles, \
    InitConfig, InitService, InitServices

t = Template()

t.add_version('2010-09-09')
t.add_description("Ansible PoC CloudFormation template\nJustin van Heerde 2015")

keyname_param = t.add_parameter(
	Parameter(
		'KeyName',
		ConstraintDescription='Must be the name of an existing EC2 KeyPair.',
		Description='Name of an existing EC2 KeyPair to enable SSH access to the instance',
		Type='AWS::EC2::KeyPair::KeyName',
	))

sshlocation_param = t.add_parameter(
	Parameter(
		'SSHLocation',
		Description='The IP address range that can be used to SSH to the EC2 instances',
		Type='String',
		MinLength='9',
		MaxLength='18',
		Default='0.0.0.0/0',
		AllowedPattern="(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})",
		ConstraintDescription=("Must be a valid IP CIDR range of the form x.x.x.x/x."),
	))

# Only allow HVM instance types for CentOS 7 AMI
instanceType_param = t.add_parameter(Parameter(
	'InstanceType',
	Type='String',
	Description='EC2 instance type',
	Default='t2.micro',
	AllowedValues=[
		't2.micro', 't2.small', 't2.medium',
		'm3.medium', 'm3.large', 'm3.xlarge', 'm3.2xlarge',
		'c3.large', 'c3.xlarge', 'c3.2xlarge', 'c3.4xlarge', 'c3.8xlarge',
		'r3.large', 'r3.xlarge', 'r3.2xlarge', 'r3.4xlarge', 'r3.8xlarge',
		'i2.xlarge', 'i2.2xlarge', 'i2.4xlarge', 'i2.8xlarge',
		'hi1.4xlarge',
		'hs1.8xlarge',
		'cr1.8xlarge',
		'cc2.8xlarge',
	],
	ConstraintDescription='must be a valid EC2 instance type.',
	))

t.add_mapping('AWSInstanceType2Arch', {
    't1.micro': {'Arch': 'PV64'},
    't2.micro': {'Arch': 'HVM64'},
    't2.small': {'Arch': 'HVM64'},
    't2.medium': {'Arch': 'HVM64'},
    'm1.small': {'Arch': 'PV64'},
    'm1.medium': {'Arch': 'PV64'},
    'm1.large': {'Arch': 'PV64'},
    'm1.xlarge': {'Arch': 'PV64'},
    'm2.xlarge': {'Arch': 'PV64'},
    'm2.2xlarge': {'Arch': 'PV64'},
    'm2.4xlarge': {'Arch': 'PV64'},
    'm3.medium': {'Arch': 'HVM64'},
    'm3.large': {'Arch': 'HVM64'},
    'm3.xlarge': {'Arch': 'HVM64'},
    'm3.2xlarge': {'Arch': 'HVM64'},
    'c1.medium': {'Arch': 'PV64'},
    'c1.xlarge': {'Arch': 'PV64'},
    'c3.large': {'Arch': 'HVM64'},
    'c3.xlarge': {'Arch': 'HVM64'},
    'c3.2xlarge': {'Arch': 'HVM64'},
    'c3.4xlarge': {'Arch': 'HVM64'},
    'c3.8xlarge': {'Arch': 'HVM64'},
    'g2.2xlarge': {'Arch': 'HVMG2'},
    'r3.large': {'Arch': 'HVM64'},
    'r3.xlarge': {'Arch': 'HVM64'},
    'r3.2xlarge': {'Arch': 'HVM64'},
    'r3.4xlarge': {'Arch': 'HVM64'},
    'r3.8xlarge': {'Arch': 'HVM64'},
    'i2.xlarge': {'Arch': 'HVM64'},
    'i2.2xlarge': {'Arch': 'HVM64'},
    'i2.4xlarge': {'Arch': 'HVM64'},
    'i2.8xlarge': {'Arch': 'HVM64'},
    'hi1.4xlarge': {'Arch': 'HVM64'},
    'hs1.8xlarge': {'Arch': 'HVM64'},
    'cr1.8xlarge': {'Arch': 'HVM64'},
    'cc2.8xlarge': {'Arch': 'HVM64'},
})

# CentOS7 AMIs
t.add_mapping('AWSRegionArch2CentOS7AMI', {
    'us-east-1': { 'HVM64': 'ami-96a818fe' },
    'us-west-2': { 'HVM64': 'ami-c7d092f7' },
    'us-west-1': { 'HVM64': 'ami-6bcfc42e' },
    'eu-central-1': { 'HVM64': 'ami-7cc4f661'},
    'eu-west-1': { 'HVM64': 'ami-e4ff5c93' },
    'ap-southeast-1': { 'HVM64': 'ami-aea582fc' },
    'ap-southeast-2': { 'HVM64': 'ami-bd523087' },
    'ap-northeast-1': { 'HVM64': 'ami-89634988' },
    'sa-east-1': { 'HVM64': 'ami-bf9520a2' },
})

# Windows 2012 AMIs
t.add_mapping('AWSRegionArch2Win2012AMI', {
    'us-east-1': { 'HVM64': 'ami-cc93a8a4' },
    'us-west-2': { 'HVM64': 'ami-8fd3f9bf' },
    'us-west-1': { 'HVM64': 'ami-6502e021' },
    'eu-central-1': { 'HVM64': 'ami-18e9d505'},
    'eu-west-1': { 'HVM64': 'ami-032b4b74' },
    'ap-southeast-1': { 'HVM64': 'ami-ae7c41fc' },
    'ap-southeast-2': { 'HVM64': 'ami-a9077a93' },
    'ap-northeast-1': { 'HVM64': 'ami-9641bb96' },
    'sa-east-1': { 'HVM64': 'ami-559b1e48' },
})

# NAT instance mapping
t.add_mapping('NatRegionMap', {
	"us-east-1": { "AMI": "ami-184dc970" },
	"us-west-1": { "AMI": "ami-a98396ec" },
	"us-west-2": { "AMI": "ami-290f4119" },
	"eu-central-1": { "AMI": "ami-ae380eb3" },
	"eu-west-1": { "AMI": "ami-14913f63" },
	"ap-southeast-1": { "AMI": "ami-6aa38238" },
	"ap-southeast-2": { "AMI": "ami-893f53b3" },
	"ap-northeast-1": { "AMI": "ami-27d6e626" },
    "sa-east-1": { "AMI": "ami-8122969c" },
})

ref_stack_id = Ref('AWS::StackId')
ref_region = Ref('AWS::Region')
ref_stack_name = Ref('AWS::StackName')

VPC = t.add_resource(VPC('AnsiblePoCVPC', CidrBlock='10.0.0.0/16', Tags=Tags(Application=ref_stack_id)))

# subnets
# make sure subnets are split between availability zones
dmzSubnet = t.add_resource(Subnet('dmzSubnet', CidrBlock='10.0.1.0/24', VpcId=Ref(VPC), AvailabilityZone=Select(0, GetAZs("")), Tags=Tags(Application=ref_stack_id)))
gitcoreSubnet = t.add_resource(Subnet('gitcoreSubnet', CidrBlock='10.0.2.0/24', VpcId=Ref(VPC), AvailabilityZone=Select(0, GetAZs("")), Tags=Tags(Application=ref_stack_id)))
managementSubnet = t.add_resource(Subnet('managementSubnet', CidrBlock='10.0.3.0/24', VpcId=Ref(VPC), AvailabilityZone=Select(1, GetAZs("")), Tags=Tags(Application=ref_stack_id)))
client1Subnet = t.add_resource(Subnet('client1Subnet', CidrBlock='10.0.10.0/24', VpcId=Ref(VPC), AvailabilityZone=Select(1, GetAZs("")), Tags=Tags(Application=ref_stack_id)))
client2Subnet = t.add_resource(Subnet('client2Subnet', CidrBlock='10.0.20.0/24', VpcId=Ref(VPC), AvailabilityZone=Select(1, GetAZs("")), Tags=Tags(Application=ref_stack_id)))

# ințernet gateway
internetGateway = t.add_resource(InternetGateway('InternetGateway', Tags=Tags(Application=ref_stack_id)))
gatewayAttachment = t.add_resource(VPCGatewayAttachment('AttachGateway', VpcId=Ref(VPC), InternetGatewayId=Ref(internetGateway)))

# route tables
dmzRouteTable = t.add_resource(RouteTable('dmzRouteTable', VpcId=Ref(VPC), Tags=Tags(Application=ref_stack_id)))
internalRouteTable = t.add_resource(RouteTable('internalRouteTable', VpcId=Ref(VPC), Tags=Tags(Application=ref_stack_id)))

# route table associations
t.add_resource(SubnetRouteTableAssociation('dmzSubnetRouteTableAssociation', SubnetId=Ref(dmzSubnet), RouteTableId=Ref(dmzRouteTable), ))
t.add_resource(SubnetRouteTableAssociation('gitcoreSubnetRouteTableAssociation', SubnetId=Ref(gitcoreSubnet), RouteTableId=Ref(internalRouteTable), ))
t.add_resource(SubnetRouteTableAssociation('managementSubnetRouteTableAssociation', SubnetId=Ref(managementSubnet), RouteTableId=Ref(internalRouteTable), ))
t.add_resource(SubnetRouteTableAssociation('client1SubnetRouteTableAssociation', SubnetId=Ref(client1Subnet), RouteTableId=Ref(internalRouteTable), ))
t.add_resource(SubnetRouteTableAssociation('client2SubnetRouteTableAssociation', SubnetId=Ref(client2Subnet), RouteTableId=Ref(internalRouteTable), ))

# security groups
customerMgmtSG = t.add_resource(
    SecurityGroup('customerMgmtSG',
    	GroupDescription="customerMgmtSG",
    	VpcId=Ref(VPC)
))

ansibleSG = t.add_resource(
    SecurityGroup('ansibleSG',
    	GroupDescription="ansibleSG",
    	SecurityGroupIngress=[
    		SecurityGroupRule(IpProtocol='tcp', SourceSecurityGroupId=Ref(customerMgmtSG), FromPort='22', ToPort='22'),
    		SecurityGroupRule(IpProtocol='tcp', SourceSecurityGroupId=Ref(customerMgmtSG), FromPort='3389', ToPort='3389'),
            SecurityGroupRule(IpProtocol='tcp', SourceSecurityGroupId=Ref(customerMgmtSG), FromPort='5985', ToPort='5986')],
    	VpcId=Ref(VPC)
))

gitpublicSG = t.add_resource(
    SecurityGroup('gitpublicSG',
    	GroupDescription="gitpublicSG",
    	SecurityGroupIngress=[
    		SecurityGroupRule(IpProtocol='tcp', CidrIp=Ref(sshlocation_param), FromPort='80', ToPort='80'),
    		SecurityGroupRule(IpProtocol='tcp', CidrIp=Ref(sshlocation_param), FromPort='443', ToPort='443'),
    	],
    	VpcId=Ref(VPC)
))

gitcoreSG = t.add_resource(
    SecurityGroup('gitcoreSG',
    	GroupDescription="gitcoreSG",
    	SecurityGroupIngress=[
    		SecurityGroupRule(IpProtocol='tcp', SourceSecurityGroupId=Ref(gitpublicSG), FromPort='443', ToPort='443'),
    	],
    	VpcId=Ref(VPC)
))

jumphostSG = t.add_resource(
    SecurityGroup('jumphostSG',
    	GroupDescription="jumphostSG",
    	SecurityGroupIngress=[
    		SecurityGroupRule(IpProtocol='tcp', CidrIp=Ref(sshlocation_param), FromPort='22', ToPort='22'),
    	],
    	VpcId=Ref(VPC)
))

mgmtSG = t.add_resource(
    SecurityGroup('mgmtSG',
    	GroupDescription="mgmtSG",
    	SecurityGroupIngress=[
    		SecurityGroupRule(IpProtocol='tcp', SourceSecurityGroupId=Ref(jumphostSG), FromPort='22', ToPort='22'),
    	],
    	VpcId=Ref(VPC)
))

NATSG = t.add_resource(
    SecurityGroup('NATSG',
    	GroupDescription="NATSG",
    	SecurityGroupIngress=[
    		SecurityGroupRule(IpProtocol='tcp', CidrIp='10.0.0.0/16', FromPort='80', ToPort='80'),
    		SecurityGroupRule(IpProtocol='tcp', CidrIp='10.0.0.0/16', FromPort='443', ToPort='443')
    	],
    	SecurityGroupEgress=[
    		SecurityGroupRule(IpProtocol='tcp', CidrIp='0.0.0.0/0', FromPort='80', ToPort='80'),
    		SecurityGroupRule(IpProtocol='tcp', CidrIp='0.0.0.0/0', FromPort='443', ToPort='443'),
    	],
    	VpcId=Ref(VPC)
))

# instances
natInstance = t.add_resource(
    Instance(
        'natInstance',
        ImageId=FindInMap('NatRegionMap', Ref('AWS::Region'), 'AMI'), InstanceType=Ref(instanceType_param),
        KeyName=Ref(keyname_param),
        SourceDestCheck=False,
        SubnetId=Ref(dmzSubnet),
        SecurityGroupIds=[Ref(NATSG), Ref(mgmtSG)],
        UserData=Base64(
            Join(
                '',
                [
                    "#!/bin/bash -v\n",
					"yum update -y aws*\n",

					"# Configure iptables\n",
					"/sbin/iptables -t nat -A POSTROUTING -o eth0 -s 0.0.0.0/0 -j MASQUERADE\n",
					"# Configure ip forwarding and redirects\n",
					"echo 1 >  /proc/sys/net/ipv4/ip_forward && echo 0 >  /proc/sys/net/ipv4/conf/eth0/send_redirects\n",
					"mkdir -p /etc/sysctl.d/\n",
					"cat <<EOF > /etc/sysctl.d/nat.conf\n",
					"net.ipv4.ip_forward = 1\n",
					"net.ipv4.conf.eth0.send_redirects = 0\n",
					"EOF\n",
					"/sbin/iptables-save > /etc/sysconfig/iptables\n",
					"\n"
                ])),
        Tags=Tags(Application=ref_stack_id),
))

jumphostInstance = t.add_resource(
    Instance(
        'jumphostInstance',
        ImageId=FindInMap('AWSRegionArch2CentOS7AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(instanceType_param), 'Arch')), InstanceType=Ref(instanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(dmzSubnet),
        SecurityGroupIds=[Ref(jumphostSG), Ref(customerMgmtSG)],
        UserData=Base64(
            Join(
                '',
                [
                    "#!/bin/bash -v\n",
					"yum update -y aws*\n",
					"\n"
                ])),
        Tags=Tags(Application=ref_stack_id),
))

gitroInstance = t.add_resource(
	Instance(
        'gitroInstance',
        ImageId=FindInMap('AWSRegionArch2CentOS7AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(instanceType_param), 'Arch')), InstanceType=Ref(instanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(dmzSubnet),
        SecurityGroupIds=[Ref(gitpublicSG), Ref(mgmtSG)],
        UserData=Base64(
            Join(
                '',
                [
                    "#!/bin/bash -v\n",
					"yum update -y aws*\n",
					"\n"
                ])),
        Tags=Tags(Application=ref_stack_id),
))

gitcoreInstance = t.add_resource(
	Instance(
        'gitcoreInstance',
        ImageId=FindInMap('AWSRegionArch2CentOS7AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(instanceType_param), 'Arch')), InstanceType=Ref(instanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(dmzSubnet),
        SecurityGroupIds=[Ref(gitcoreSG), Ref(mgmtSG)],
        UserData=Base64(
            Join(
                '',
                [
                    "#!/bin/bash -v\n",
					"yum update -y aws*\n",
					"\n"
                ])),
        Tags=Tags(Application=ref_stack_id),
))

mgmtInstance = t.add_resource(
	Instance(
        'mgmtInstance',
        ImageId=FindInMap('AWSRegionArch2CentOS7AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(instanceType_param), 'Arch')), InstanceType=Ref(instanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(managementSubnet),
        SecurityGroupIds=[Ref(customerMgmtSG), Ref(mgmtSG)],
        UserData=Base64(
            Join(
                '',
                [
                    "#!/bin/bash -v\n",
					"yum update -y aws*\n",
					"\n"
                ])),
        Tags=Tags(Application=ref_stack_id),
))

client1app1Instance = t.add_resource(
	Instance(
        'client1app1Instance',
        ImageId=FindInMap('AWSRegionArch2CentOS7AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(instanceType_param), 'Arch')), InstanceType=Ref(instanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(client1Subnet),
        SecurityGroupIds=[Ref(ansibleSG)],
        UserData=Base64(
            Join(
                '',
                [
                    "#!/bin/bash -v\n",
					"yum update -y aws*\n",
					"\n"
                ])),
        Tags=Tags(Application=ref_stack_id),
))

client1app2Instance = t.add_resource(
	Instance(
        'client1app2Instance',
        ImageId=FindInMap('AWSRegionArch2Win2012AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(instanceType_param), 'Arch')), InstanceType=Ref(instanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(client1Subnet),
        SecurityGroupIds=[Ref(ansibleSG)],
        Tags=Tags(Application=ref_stack_id),
))

client2app1Instance = t.add_resource(
	Instance(
        'client2app1Instance',
        ImageId=FindInMap('AWSRegionArch2CentOS7AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(instanceType_param), 'Arch')), InstanceType=Ref(instanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(client1Subnet),
        SecurityGroupIds=[Ref(ansibleSG)],
        UserData=Base64(
            Join(
                '',
                [
                    "#!/bin/bash -v\n",
					"yum update -y aws*\n",
					"\n"
                ])),
        Tags=Tags(Application=ref_stack_id),
))

client2app2Instance = t.add_resource(
	Instance(
        'client2app2Instance',
        ImageId=FindInMap('AWSRegionArch2Win2012AMI', Ref('AWS::Region'), FindInMap(
                'AWSInstanceType2Arch', Ref(instanceType_param), 'Arch')), InstanceType=Ref(instanceType_param), KeyName=Ref(keyname_param),
        SubnetId=Ref(client1Subnet),
        SecurityGroupIds=[Ref(ansibleSG)],
        Tags=Tags(Application=ref_stack_id),
))

# Elastic IPs
jh1EIP = t.add_resource(EIP('jh1EIP', DependsOn='AttachGateway', Domain='vpc', InstanceId=Ref(jumphostInstance)))
gitroEIP = t.add_resource(EIP('gitroEIP', DependsOn='AttachGateway', Domain='vpc', InstanceId=Ref(gitroInstance)))
natEIP = t.add_resource(EIP('natEIP', DependsOn='AttachGateway', Domain='vpc', InstanceId=Ref(natInstance)))

# routes
t.add_resource(Route('dmzRoute1', DependsOn='AttachGateway', GatewayId=Ref(internetGateway), DestinationCidrBlock='0.0.0.0/0', RouteTableId=Ref(dmzRouteTable)))
t.add_resource(Route('internalRoute1', DependsOn='natInstance', InstanceId=Ref(natInstance), DestinationCidrBlock='0.0.0.0/0', RouteTableId=Ref(internalRouteTable)))

# Outputs
t.add_output([Output('jumphostEIP', Description='Jumphost EIP', Value=Ref(jh1EIP))])
t.add_output([Output('gitroEIP', Description='Git read-only EIP', Value=Ref(gitroEIP))])
t.add_output([Output('natEIP', Description='NAT instance EIP', Value=Ref(natEIP))])

print(t.to_json())
